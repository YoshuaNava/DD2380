#ifndef _TREESEARCH_HPP_
#define _TREESEARCH_HPP_

#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include "gamestate.hpp"
#include "move.hpp"


#define PSEUDO_INFINITY 100000000000000000.0
#define HASHSIZE 5000000

bool hash_table_states[HASHSIZE];
double hash_table_utility[HASHSIZE];
std::hash<std::string> str_hash;


namespace checkers
{

class TreeSearch
{
public:
    const static bool verbose = false;
    const static int max_depth = 9;


    static double utility(GameState state, uint8_t my_player, int depth)
    {
        double red_pieces=0, white_pieces=0;
        double utility_val = 0;
        for (int i = 0; i < state.cSquares; ++i)
        {
            uint8_t cell_content = state.at(i);
            if (cell_content & CELL_RED)
            {
                if(cell_content & (CELL_RED | CELL_KING))
                    red_pieces += 5;
                else
                    red_pieces++;
            }
            else if (state.at(i) & CELL_WHITE)
            {
                if(cell_content & (CELL_WHITE | CELL_KING))
                    white_pieces += 5;
                else
                    white_pieces++;
            }
        }

        if(my_player == CELL_RED)
        {
            utility_val += red_pieces - white_pieces;
            if(state.isRedWin())
            {
                utility_val += 15 - depth;
            }
            else if(state.isWhiteWin())
            {
                utility_val -= 15 + depth;
            }
        }

        if(my_player == CELL_WHITE)
        {
            utility_val += white_pieces - red_pieces;
            if(state.isWhiteWin())
            {
                utility_val += 15 - depth;
            }
            else if(state.isRedWin())
            {
                utility_val -= 15 + depth;
            }
        }


        if(state.isDraw())
        {
            utility_val += 5;
        }


        return utility_val;
    }

	static unsigned long getHashTable(GameState state, uint8_t my_player)
	{
		std::string map(63, '0');
		int l = 0;
		for (int i = 0; i < 64; i++)
		{
			map[i] = state.at(i);
		}

		return str_hash(map);
	}


    // Following the minimax recommended implementation in page 6 of the assignment statement
    static double minimax(GameState curr_state, uint8_t my_player, int depth)
    {
        if(curr_state.isEOG() || depth >= max_depth)
        {
            return utility(curr_state, my_player, depth);
        }

        depth++;

        std::vector<GameState> future_states;
        curr_state.findPossibleMoves(future_states);

        if(curr_state.getNextPlayer() == my_player)
        {
            double best_utility = -PSEUDO_INFINITY;
            double v;
            for (GameState future_state : future_states)
            {
				unsigned long hashvalue = getHashTable(future_state, my_player);
				if(hash_table_states[hashvalue % HASHSIZE])
					continue;

				hash_table_states[hashvalue % HASHSIZE] = true;

                v = minimax(future_state, my_player, depth);
                best_utility = std::max(v, best_utility);
            }
            return best_utility;
        }
        else
        {
            double best_utility = PSEUDO_INFINITY;
            double v;
            for (GameState future_state : future_states)
            {
				unsigned long hashvalue = getHashTable(future_state, my_player);
				if(hash_table_states[hashvalue % HASHSIZE])
					continue;

				hash_table_states[hashvalue % HASHSIZE] = true;

                v = minimax(future_state, my_player, depth);
                best_utility = std::min(v, best_utility);
            }
            return best_utility;
        }
    }





    // Following the alpha-beta pruning recommended implementation in page 7 of the assignment statement
    static double alphaBeta(GameState curr_state, uint8_t my_player, int depth, double alpha, double beta)
    {
        if(curr_state.isEOG() || depth >= max_depth)
        {
            return utility(curr_state, my_player, depth);
        }

        depth++;

        std::vector<GameState> future_states;
        curr_state.findPossibleMoves(future_states);
        double v;

        if(curr_state.getNextPlayer() == my_player)
        {
            v = -PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
                v = std::max(v, alphaBeta(future_state, my_player, depth, alpha, beta));
                alpha = std::max(alpha, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }
        else
        {
            v = PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
                v = std::min(v, alphaBeta(future_state, my_player, depth, alpha, beta));
                beta = std::min(beta, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }

        return v;
    }


};

};


#endif