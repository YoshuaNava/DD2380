#include "player.hpp"
#include <cstdlib>

#include "TreeSearch.hpp"

namespace checkers
{

GameState Player::play(const GameState &pState,const Deadline &pDue)
{
    //std::cerr << "Processing " << pState.toMessage() << std::endl;

    std::vector<GameState> lNextStates;
    pState.findPossibleMoves(lNextStates);

    if (lNextStates.size() == 0) return GameState(pState, Move());

    /*
     * Here you should write your clever algorithms to get the best next move, ie the best
     * next state. This skeleton returns a random move instead.
     */
    srand (time(NULL));
    int depth = 0;
    uint8_t my_player = pState.getNextPlayer();
    double max_utility = -PSEUDO_INFINITY;
    int best_move_idx = -1;
    int i = 0;
	std::vector<GameState> future_states;
    pState.findPossibleMoves(future_states);

    // For building a random agent, just comment out the for-each loop below
    double alpha, beta, utility;
    alpha = -PSEUDO_INFINITY;
    beta = PSEUDO_INFINITY;

    for (GameState future_state : future_states)
    {

    	// utility = TreeSearch::minimax(future_state, my_player, depth);
        utility = TreeSearch::alphaBeta(future_state, my_player, depth, alpha, beta);

        if(utility > max_utility)
        {
            max_utility = utility;
            best_move_idx = i;
        }
        i++;


        if(TreeSearch::verbose)
        {
        	std::cerr << "*********** Future state\n";
        	std::cerr << future_state.toString(my_player);
        	std::cerr << "Utility: " << utility << std::endl;
        }
    }

    if(TreeSearch::verbose)
    {
	   std::cerr << "################################################# Best move index: " << best_move_idx << std::endl;
    }
    
    if(best_move_idx == -1)
		return lNextStates[rand() % lNextStates.size()];
	else
    	return lNextStates[best_move_idx];
}

/*namespace checkers*/ }
