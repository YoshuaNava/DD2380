clc;
close all;
clear all;

%% Horizontal rows facing viewer (16)
disp('Horizontal rows facing viewer (16)')
board = zeros(4,4,4);

row = 1;

for layer=1:4
    for i=1:4
        board(i, :, layer) = row;
        row = row + 1;
    end
end

board


%% Vertical rows facing viewer (16)
disp('Vertical rows facing viewer (16)')
board = zeros(4,4,4);

row = 1;

for layer=1:4
    for j=1:4
        board(:, j, layer) = row;
        row = row + 1;
    end    
end

board


%% Horizontal rows facing side (16)
disp('Horizontal rows facing side (16)')
board = zeros(4,4,4);

row = 1;

for i=1:4
    for j=1:4
        for layer=1:4
            board(i, j, layer) = row;
        end
        row = row + 1;
    end
end

board


%% Diagonals facing viewer (4)
disp('Diagonals facing viewer (4)')
board = zeros(4,4,4);

diagonal = 1;

for layer=1:4
        for i=1:4
            board(i, i, layer) = diagonal;
        end
        diagonal = diagonal + 1;
end

board


%% Anti-diagonals facing viewer (4)
disp('Anti-diagonals facing viewer (4)')
board = zeros(4,4,4);

diagonal = 1;

for layer=1:4
        for i=1:4
            board(i, 4-i+1, layer) = diagonal;
        end
        diagonal = diagonal + 1;
end

board

%% Diagonals facing side (4)
disp('Diagonals facing side (4)')
board = zeros(4,4,4);

diagonal = 1;

for j=1:4
    for i=1:4
        board(i, j, 4-i+1) = diagonal;
    end
    diagonal = diagonal + 1;
end

board


%% Anti-diagonals facing side (4)
disp('Anti-diagonals facing side (4)')
board = zeros(4,4,4);

diagonal = 1;

for j=1:4
    for i=1:4
        board(i, j, i) = diagonal;
    end
    diagonal = diagonal + 1;
end

board


%% Diagonals facing bottom (4)
disp('Diagonals facing floor (4)')
board = zeros(4,4,4);

diagonal = 1;

for i=1:4
    for j=1:4
        board(i, j, 4-j+1) = diagonal;
    end
    diagonal = diagonal + 1;
end

board


%% Anti-diagonals facing bottom (4)
disp('Anti-diagonals facing floor (4)')
board = zeros(4,4,4);

diagonal = 1;

for i=1:4
    for j=1:4
        board(i, j, j) = diagonal;
    end
    diagonal = diagonal + 1;
end

board


%% Cube diagonals (2)
disp('Cube diagonals (2)')
board = zeros(4,4,4);

diagonal = 1;

for i=1:4
    board(i, i, i) = diagonal;
end

diagonal = diagonal + 1;

for i=1:4
    board(4-i+1, i, i) = diagonal;
end


board

