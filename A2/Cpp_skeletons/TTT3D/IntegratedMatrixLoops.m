clc;
close all;
clear all;

%% Integrated matrix traversing loop.

% Variables that will contain the traversing patterns, for visualization
horz_rows_front = zeros(4,4,4);
horz_rows_side = zeros(4,4,4);
vertical_rows = zeros(4,4,4);
front_diagonal = zeros(4,4,4);
front_antidiagonal = zeros(4,4,4);
side_diagonal = zeros(4,4,4);
side_antidiagonal = zeros(4,4,4);
bottom_diagonal = zeros(4,4,4);
bottom_antidiagonal = zeros(4,4,4);
cube_diagonals = zeros(4,4,4);


% These variables store the row or diagonal number that we are traversing.
% However, we could also use them to count the number of X's (or O's,
% depending on which player we represent in the game), along each row/diagonal.
% Therefore, if the path is clear from opponents and has N of our marks, we 
% give this state a reward 10^N.
% If at any moment we find a mark from the opponent in the way, we say that
% the reward will be 0.
hf_row = 1;
hs_row = 1;
vert_row = 1;
df = 1;
adf = 1;
ds = 1;
ads = 1;
db = 1;
adb = 1;
dc = 1;


% The loop
for i=1:4
    cube_diagonals(i, i, i) = dc;
    cube_diagonals(4-i+1, i, i) = dc + 1;
    
    for j=1:4
        for k=1:4
            horz_rows_front(j, k, i) = hf_row;
            horz_rows_side(i, j, k) = hs_row;
            vertical_rows(k, j, i) = vert_row;
        end
        
        front_diagonal(j, j, i) = df;
        front_antidiagonal(j, 4-j+1, i) = adf;
        
        side_diagonal(j, i, 4-j+1) = ds;
        side_antidiagonal(j, i, j) = ads;
        
        bottom_diagonal(i, j, 4-j+1) = db;
        bottom_antidiagonal(i, j, j) = adb;
        
        hf_row = hf_row + 1;
        hs_row = hs_row + 1;
        vert_row = vert_row + 1;
    end
    df = df + 1;
    adf = adf + 1;
    ds = ds + 1;
    ads = ads + 1;
    db = db + 1;
    adb = adb + 1;
end



% This part of the code is just here for displaying the results
horz_rows_front
horz_rows_side
vertical_rows
front_diagonal
front_antidiagonal
side_diagonal
side_antidiagonal
bottom_diagonal
bottom_antidiagonal
cube_diagonals