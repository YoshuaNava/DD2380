#ifndef _TREESEARCH_HPP_
#define _TREESEARCH_HPP_

#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include "gamestate.hpp"
#include "move.hpp"


#define PSEUDO_INFINITY 100000000000000000.0


namespace TICTACTOE
{

class TreeSearch
{
public:

    const static bool verbose = true;


    static double utility(GameState state, uint8_t my_player, int depth)
    {
        double utility_val = 0;

        if((my_player == CELL_O) && (state.isOWin()))
        {
            utility_val += 30;
        }
        else if((my_player == CELL_O) && (state.isXWin()))
        {
            utility_val += -30;
        }

        if((my_player == CELL_X) && (state.isXWin()))
        {
            utility_val += 30;
        }
        else if((my_player == CELL_X) && (state.isOWin()))
        {
            utility_val += -30;
        }

        if(state.isDraw())
        {
            utility_val += 5;
        }

        return utility_val;
    }


    // Following the minimax recommended implementation in page 6 of the assignment statement
    static double minimax(GameState curr_state, uint8_t my_player, int depth)
    {
        if(curr_state.isEOG() || depth <= 0)
        {
            return utility(curr_state, my_player, depth);
        }

        depth--;

        std::vector<GameState> future_states;
        curr_state.findPossibleMoves(future_states);

        if(curr_state.getNextPlayer() == my_player)
        {
            double best_utility = -PSEUDO_INFINITY;
            double v;
            for (GameState future_state : future_states)
            {
                v = minimax(future_state, my_player, depth);
                best_utility = std::max(v, best_utility);
            }
            return best_utility;
        }
        else
        {
            double best_utility = PSEUDO_INFINITY;
            double v;
            for (GameState future_state : future_states)
            {
                v = minimax(future_state, my_player, depth);
                best_utility = std::min(v, best_utility);
            }
            return best_utility;
        }
    }





    // Following the alpha-beta pruning recommended implementation in page 7 of the assignment statement
    static double alphaBeta(GameState curr_state, uint8_t my_player, int depth, double alpha, double beta)
    {
        if(curr_state.isEOG() || depth <= 0)
        {
            return utility(curr_state, my_player, depth);
        }

        depth--;

        std::vector<GameState> future_states;
        curr_state.findPossibleMoves(future_states);
        double v;

        if(curr_state.getNextPlayer() == my_player)
        {
            v = -PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
                v = std::max(v, alphaBeta(future_state, my_player, depth, alpha, beta));
                alpha = std::max(alpha, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }
        else
        {
            v = PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
                v = std::min(v, alphaBeta(future_state, my_player, depth, alpha, beta));
                beta = std::min(beta, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }

        return v;
    }


};

};


#endif