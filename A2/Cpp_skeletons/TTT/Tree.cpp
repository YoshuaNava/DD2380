#ifndef _TREE_HPP_
#define _TREE_HPP_

#include <cstdlib>
#include <iostream>
#include <vector>
#include "gamestate.hpp"
#include "move.hpp"



namespace TICTACTOE
{
	class Node
	{
	public:
		Node* parent = NULL;
		std::vector<Node> children;
		GameState game_state;
		Move move;
	};
};

#endif

