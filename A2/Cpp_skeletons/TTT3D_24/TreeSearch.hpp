#ifndef _TREESEARCH_HPP_
#define _TREESEARCH_HPP_

#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>
#include "gamestate.hpp"
#include "move.hpp"
#include <typeinfo>

#define PSEUDO_INFINITY 100000000000000000.0
#define VICTORY_SCREEEEEECH 10000
#define HASHSIZE 500000000

bool hashtable[HASHSIZE];
bool hashtableUtil[HASHSIZE];
std::hash<std::string> str_hash;

namespace TICTACTOE3D
{

class TreeSearch
{

public:
    const static bool verbose = false;
	const static int max_depth = 2;
	const static int valuable_score = 200;


	static double valueFunction(double i)
	{
		double p0 = 1;
		double p1 = 10;
		double p2 = 100;
		double p3 = 1000;
		if (i == 0)
			return p0;
		else if (i == p0)
			return p1;
		else if (i == p1)
			return p2;
		else if (i == p2)
			return p3;
		return 0;
	}


    static double utility(GameState state, uint8_t my_player, int depth)
    {
        double utility_val = 0;

		for (int i = 0; i < 3; ++i)
		{
			utility_val += getLineValue(state, my_player, i);
			utility_val += getPositiveDiagonal(state, my_player, i);
			utility_val += getNegativeDiagonal(state, my_player, i);
		}
		utility_val += getMainDiagonal(state, my_player);
		utility_val += getCounterDiagonal(state, my_player, 0);
		utility_val += getCounterDiagonal(state, my_player, 1);
		utility_val += getSecondDiagonal(state, my_player);

        // double board_value = boardLoops(state, my_player);
        // std::cerr << "board value " << board_value << std::endl;
        // std::cerr << state.toString(my_player) << std::endl;

        // utility_val += board_value;


		if ((my_player == CELL_O) && (state.isOWin()))
			utility_val += VICTORY_SCREEEEEECH;
		else if ((my_player == CELL_O) && (state.isXWin()))
			utility_val += -VICTORY_SCREEEEEECH;

		if ((my_player == CELL_X) && (state.isXWin()))
			utility_val += VICTORY_SCREEEEEECH;
		else if ((my_player == CELL_X) && (state.isOWin()))
			utility_val += -VICTORY_SCREEEEEECH;

		if (state.isDraw())
			utility_val += 5;

		utility_val += 10;

		return utility_val;
    }


	static unsigned long getHashTable(GameState state, uint8_t my_player)
	{
		std::string map(63, '0');
		int l = 0;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					map[l] = state.at(i, j, k);
					l++;
				}
			}
		}
		return str_hash(map);

	}

    // Following the alpha-beta pruning recommended implementation in page 7 of the assignment statement
    static double alphaBeta(GameState curr_state, uint8_t my_player, int depth, double alpha, double beta, double v)
    {
		if (curr_state.isEOG() || (depth >= max_depth) || (depth >= 1 && v < valuable_score))
		{
			return utility(curr_state, my_player, depth);
		}

		depth++;

        std::vector<GameState> future_states;
        curr_state.findPossibleMoves(future_states);

        if(curr_state.getNextPlayer() == my_player)
        {
            v = -PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
				unsigned long hashvalue = getHashTable(future_state, my_player);
				if(hashtable[hashvalue%HASHSIZE])
					continue;

				hashtable[hashvalue%HASHSIZE] = true;
                v = std::max(v, alphaBeta(future_state, my_player, depth, alpha, beta, v));
                alpha = std::max(alpha, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }
        else
        {
            v = PSEUDO_INFINITY;
            for (GameState future_state : future_states)
            {
				unsigned long hashvalue = getHashTable(future_state, my_player);
				if (hashtable[hashvalue%HASHSIZE])
					continue;

				hashtable[hashvalue%HASHSIZE] = true;
                v = std::min(v, alphaBeta(future_state, my_player, depth, alpha, beta, v));
                beta = std::min(beta, v);
                if(beta <= alpha)
                {
                    break;
                }
            }
        }

        return v;
    }


	//get Columns, Rows, and Layers
	static double getLineValue(GameState state, uint8_t my_player, int caseStyle)
	{
		double value = 0;
		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		double contents;
		if (caseStyle < 3)
		{
			for (int i = 0; i < 4; ++i)	//for each layer
			{
				for (int j = 0; j < 4; ++j) //for each row
				{

					double row_count = 0, enemy_count = 0;
					double enemy_row = 1, friendly_row = 1;
					for (int k = 0; k < 4; k++) //for each column
					{
						switch (caseStyle) {
						case 0:
							contents = state.at(state.rowColLayToCell(i, j, k));
							break;
						case 1:
							contents = state.at(state.rowColLayToCell(j, k, i));
							break;
						case 2:
							contents = state.at(state.rowColLayToCell(k, j, i));
							break;
						}
						if (contents == my_player)
						{
							row_count = valueFunction(row_count);
							friendly_row = 0;
						}
						if (contents == enemy)
						{
							enemy_count = valueFunction(enemy_count);
							enemy_row = 0;
						}
					}
					value += row_count * enemy_row;
					value -= enemy_count * friendly_row;
				}
			}
		}
		return value;
	}



	//get diagonals that share a value
	static double getPositiveDiagonal(GameState state, uint8_t my_player, int caseStyle)
	{
		double value = 0;
		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		double contents;

			for (int i = 0; i < 4; ++i) //for each row
			{

				double row_count = 0, enemy_count = 0;
				double enemy_row = 1, friendly_row = 1;
				for (int j = 0; j < 4; j++) //for each column
				{
					switch (caseStyle) {
					case 0:
						contents = state.at(state.rowColLayToCell(i, j, j));
						break;
					case 1:
						contents = state.at(state.rowColLayToCell(j, j, i));
						break;
					case 2:
						contents = state.at(state.rowColLayToCell(j, i, j));
						break;
					}
					if (contents == my_player)
					{
						row_count = valueFunction(row_count);
						friendly_row = 0;
					}
					if (contents == enemy)
					{
						enemy_count = valueFunction(enemy_count);
						enemy_row = 0;
					}
				}
				value += row_count * enemy_row;
				value -= enemy_count * friendly_row;
			}
		return value;
	}


	//get value for diagonals that need a neative for loop
	static double getNegativeDiagonal(GameState state, uint8_t my_player, int caseStyle)
	{
		double value = 0;
		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		double contents;
		if (caseStyle < 3)
		{
			for (int i = 0; i < 4; ++i)	//for each layer
			{
				double row_count = 0, enemy_count = 0;
				double enemy_row = 1, friendly_row = 1;
				for (int j = 0; j < 4; ++j) //for each row
				{
					switch (caseStyle) {
					case 0:
						contents = state.at(state.rowColLayToCell(3-j, j, i));
						break;
					case 1:
						contents = state.at(state.rowColLayToCell(i, j, 3 - j));
						break;
					case 2:
						contents = state.at(state.rowColLayToCell(3 - j, i, j));
						break;
					}
					if (contents == my_player)
					{
						row_count = valueFunction(row_count);
						friendly_row = 0;
					}
					if (contents == enemy)
					{
						enemy_count = valueFunction(enemy_count);
						enemy_row = 0;
					}
				}
				value += row_count * enemy_row;
				value -= enemy_count * friendly_row;
			}
		}
		return value;
	}


	//get value for diagonals that need a neative for loop
	static double getMainDiagonal(GameState state, uint8_t my_player)
	{
		double value = 0;
		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		double contents;
		double row_count = 0, enemy_count = 0;
		double enemy_row = 1, friendly_row = 1;
			for (int i = 0; i < 4; ++i)	//for each layer
			{
				contents = state.at(state.rowColLayToCell(i, i, i));
				if (contents == my_player)
				{
					row_count = valueFunction(row_count);
					friendly_row = 0;
				}
				if (contents == enemy)
				{
					enemy_count = valueFunction(enemy_count);
					enemy_row = 0;
				}
			}
		value += row_count * enemy_row;
		value -= enemy_count * friendly_row;
		return value;
	}

	//get value for diagonals that need a neative for loop
	static double getSecondDiagonal(GameState state, uint8_t my_player)
	{
		double value = 0;
		double player, enemy;
		if (my_player == CELL_X)
		{
			player = CELL_X;
			enemy = CELL_O;
		}
		else
		{
			enemy = CELL_X;
			player = CELL_O;
		}
		double contents;
		double row_count = 0, enemy_count = 0;
		double enemy_row = 1, friendly_row = 1;
		for (int i = 0; i < 4; ++i)	//for each layer
		{
			for (int j = 0; j < 0; j++) {
				contents = state.at(state.rowColLayToCell(j, j, i));
				if (contents == player)
				{
					row_count = valueFunction(row_count);
					friendly_row = 0;
				}
				if (contents == enemy)
				{
					enemy_count = valueFunction(enemy_count);
					enemy_row = 0;
				}
			}

		}
		value += row_count * enemy_row;
		value -= enemy_count * friendly_row;
		return value;
	}

	

	//get value for diagonals that need a neative for loop
	static double getCounterDiagonal(GameState state, uint8_t my_player, int caseStyle)
	{
		double value = 0;
		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		double contents;
		double row_count = 0, enemy_count = 0;
		double enemy_row = 1, friendly_row = 1;
		if (caseStyle < 3)
		{
				for (int j = 0; j < 4; ++j) //for each row
				{
					switch (caseStyle) {
					case 0:
						contents = state.at(state.rowColLayToCell(3-j, j, j));
						break;
					case 1:
						contents = state.at(state.rowColLayToCell(j, 3 - j, j));
						break;
					}
					if (contents == my_player)
					{
						row_count = valueFunction(row_count);
						friendly_row = 0;
					}
					if (contents == enemy)
					{
						enemy_count = valueFunction(enemy_count);
						enemy_row = 0;
					}
				}
			}
		value += row_count * enemy_row;
		value -= enemy_count * friendly_row;
		return value;
	}



	static double boardLoops(GameState state, uint8_t my_player)
	{
		/* 10 possible ways to loop around the board matrix:
			 0: Front rows
			 1: Side rows
			 2: Vertical cols
			 3: Front diagonal
			 4: Front antidiagonal
			 5: Side diagonal
			 6: Side antidiagonal
			 7: Bottom diagonal
			 8: Bttom antidiagonal
			 9: Cube diagonals
			 10: Cube antidiagonals
		 One value of reward per each*/
		double total_reward = 0;
		double reward[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int friends_count[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int enemy_count[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		double power_base = 10;

		uint8_t enemy = my_player == CELL_X ? CELL_O : CELL_X;

		// std::cerr << "hola" << std::endl;

		for(int i=0; i<4 ;i++)
		{
			friends_count[3] = 0;
			friends_count[4] = 0;
			friends_count[5] = 0;
			friends_count[6] = 0;
			friends_count[7] = 0;
			friends_count[8] = 0;
			enemy_count[3] = 0;
			enemy_count[4] = 0;
			enemy_count[5] = 0;
			enemy_count[6] = 0;
			enemy_count[7] = 0;
			enemy_count[8] = 0;

			for(int j=0; j<4 ;j++)
			{
				friends_count[0] = 0;
				friends_count[1] = 0;
				friends_count[2] = 0;
				enemy_count[0] = 0;
				enemy_count[1] = 0;
				enemy_count[2] = 0;

				// Front rows, side rows and vertical cols
				for(int k=0; k<4 ;k++)
				{
					if(state.at(state.rowColLayToCell(j, k, i)) == my_player) friends_count[0]++;
					else if (state.at(state.rowColLayToCell(j, k, i)) == enemy) enemy_count[0]++;

					if(state.at(state.rowColLayToCell(i, j, k)) == my_player) friends_count[1]++;
					else if (state.at(state.rowColLayToCell(i, j, k)) == enemy) enemy_count[1]++;

					if(state.at(state.rowColLayToCell(k, j, i)) == my_player) friends_count[2]++;
					else if (state.at(state.rowColLayToCell(k, j, i)) == enemy) enemy_count[2]++;
				}

				reward[0] += (enemy_count[0] == 0 ? pow(power_base, friends_count[0]) : -pow(power_base, enemy_count[0]));
				reward[1] += (enemy_count[1] == 0 ? pow(power_base, friends_count[1]) : -pow(power_base, enemy_count[1]));
				reward[2] += (enemy_count[2] == 0 ? pow(power_base, friends_count[2]) : -pow(power_base, enemy_count[2]));


				if(state.at(state.rowColLayToCell(j, j, i)) == my_player) friends_count[3]++;
				else if (state.at(state.rowColLayToCell(j, j, i)) == enemy) enemy_count[3]++;

				if(state.at(state.rowColLayToCell(j, 3-j, i)) == my_player) friends_count[4]++;
				else if (state.at(state.rowColLayToCell(j, 3-j, i)) == enemy) enemy_count[4]++;

				if(state.at(state.rowColLayToCell(j, i, 3-j)) == my_player) friends_count[5]++;
				else if (state.at(state.rowColLayToCell(j, i, 3-j)) == enemy) enemy_count[5]++;

				if(state.at(state.rowColLayToCell(j, i, j)) == my_player) friends_count[6]++;
				else if (state.at(state.rowColLayToCell(j, i, j)) == enemy) enemy_count[6]++;

				if(state.at(state.rowColLayToCell(i, j, 3-j)) == my_player) friends_count[7]++;
				else if (state.at(state.rowColLayToCell(i, j, 3-j)) == enemy) enemy_count[7]++;

				if(state.at(state.rowColLayToCell(i, j, j)) == my_player) friends_count[8]++;
				else if (state.at(state.rowColLayToCell(i, j, j)) == enemy) enemy_count[8]++;

			}

			reward[3] += (enemy_count[3] == 0 ? pow(power_base, friends_count[3]) : -pow(power_base, enemy_count[3]));
			reward[4] += (enemy_count[4] == 0 ? pow(power_base, friends_count[4]) : -pow(power_base, enemy_count[4]));
			reward[5] += (enemy_count[5] == 0 ? pow(power_base, friends_count[5]) : -pow(power_base, enemy_count[5]));
			reward[6] += (enemy_count[6] == 0 ? pow(power_base, friends_count[6]) : -pow(power_base, enemy_count[6]));
			reward[7] += (enemy_count[7] == 0 ? pow(power_base, friends_count[7]) : -pow(power_base, enemy_count[7]));
			reward[8] += (enemy_count[8] == 0 ? pow(power_base, friends_count[8]) : -pow(power_base, enemy_count[8]));

			if(state.at(state.rowColLayToCell(i, i, i)) == my_player) friends_count[9]++;
			else if (state.at(state.rowColLayToCell(i, i, i)) == enemy) enemy_count[9]++;

			if(state.at(state.rowColLayToCell(3-i, i, i)) == my_player) friends_count[10]++;
			else if (state.at(state.rowColLayToCell(3-i, i, i)) == enemy) enemy_count[10]++;
		}

		reward[9] += (enemy_count[9] == 0 ? pow(power_base, friends_count[9]) : -pow(power_base, enemy_count[9]));
		reward[10] += (enemy_count[10] == 0 ? pow(power_base, friends_count[10]) : -pow(power_base, enemy_count[10]));

		for(int i=0; i<11 ;i++)
		{
			total_reward += reward[i];
			// std::cerr << "Reward pos " << i << " is " << reward[i] << std::endl;
		}

		return total_reward;
	}




};


};


#endif