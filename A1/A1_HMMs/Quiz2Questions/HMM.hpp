#ifndef _HMM_HPP_
#define _HMM_HPP_


#include <time.h> 
#include <math.h>
#include <vector>
#include <iostream>
#include <random>
#include "Matrix.hpp"


class HiddenMarkovModel
{
public:

	std::string bird_type;
	int id;
	Matrix A, B, pi;
	int T = 99;
	int N = 3;
	int K = 4;
	double epsilon = 0.000000001;
	std::vector<int> train_obs_seq;
	std::vector<int> test_obs_seq;
	int* hidden_states_seq;
	Matrix alpha, beta;
	std::vector<Matrix> digammas, deltas;
	Matrix C_t, gammas, new_A, new_B, new_pi;
	int max_obs = 500;
	int max_iter = 100;
	int iter = 0;
	bool trained = false;
	double log_prob, old_log_prob = -10000000;


	void initialize(std::string model_name, int species_id = -1)
	{
		id = species_id;
		bird_type = model_name;
		pi = new Matrix(1, N);
		A = new Matrix(N, N);
		B = new Matrix(N, K);
		
		double scalePi = 0.0;
		double scaleA = 0.0;
		double scaleB = 0.0;
		for (int i = 0; i < N; i++)
		{

			pi.cells[0][i] = (1.0f + 10.0f*((double)std::rand() / (double)(RAND_MAX))) / ((double)N);
			scalePi += pi.cells[0][i];

			for (int j = 0; j < K; j++)
			{
				if (j < N)
				{
					A.cells[i][j] = (1.0f + 10.0f*((double)std::rand() / (double)(RAND_MAX))) / ((double)N);
					scaleA += A.cells[i][j];
				}
					

				B.cells[i][j] = (1.0f + 10.0f*((double) std::rand() / (double)(RAND_MAX)))/ ((double) K);
				scaleB += B.cells[i][j];
			}

			for (int j = 0; j < K; j++)
			{
				if (j < N)
					A.cells[i][j] /= scaleA;
				B.cells[i][j] /= scaleB;
			}
			scaleA = 0;
			scaleB = 0;
		}
		pi = pi / scalePi;

		train_obs_seq = std::vector<int>();
		test_obs_seq = std::vector<int>();
		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}



	void reinitializeLambda()
	{
		pi = new Matrix(1, N);
		A = new Matrix(N, N);
		B = new Matrix(N, K);
		
		srand (time(NULL));

		double scalePi = 0.0;
		double scaleA = 0.0;
		double scaleB = 0.0;
		for (int i = 0; i < N; i++)
		{
			pi.cells[0][i] = (1.0f + ((double)std::rand() / (double)(RAND_MAX))) / (double)N;
			scalePi += pi.cells[0][i];
			for (int j = 0; j < K; j++)
			{
				if (j < N)
				{
					A.cells[i][j] = (1.0f + ((double)std::rand() / (double)(RAND_MAX))) / ((double)N);

					if(i==j)
						A.cells[i][j] *= 2;

					scaleA += A.cells[i][j];
				}
					

				B.cells[i][j] = (1.0f + ((double) std::rand() / (double)(RAND_MAX)))/ (double) K;

				if(i==j)
					B.cells[i][j] *= 2;

				scaleB += B.cells[i][j];
			}
			for (int j = 0; j < K; j++)
			{
				if (j < N)
					A.cells[i][j] /= scaleA;
				
				B.cells[i][j] /= scaleB;
			}
			scaleA = 0;
			scaleB = 0;
		}
		pi = pi / scalePi;
	}

	void reinitializeBaumWelchParams()
	{
		if(train_obs_seq.size() > 0)
			T = train_obs_seq.size();

		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}


	void reinitializeBaumWelchParamsTest()
	{
		if(test_obs_seq.size() > 0)
			T = test_obs_seq.size();

		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}

	void runForwardAlgorithmTest()
	{
		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][test_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][test_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "alpha" << std::endl;
		// alpha.displayFullInfo();
	}


	void runFutureStateEstimation()
	{
		double numer, denom;

		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][test_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][test_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}
	
			// Make beta equal to the scaling factors
		for(int i=0; i<N ;i++)
		{
			if(C_t.cells[0][T-1] != 0)
				beta.cells[T-1][i] = C_t.cells[0][T-1];
		}

		// Beta-pass
		for(int t=T-2; t>=0 ;t--)
		{
			for (int i = 0; i<N ;i++)
			{
				beta.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					beta.cells[t][i] += A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j];
				}
				
				if(C_t.cells[0][t] != 0.0)
					beta.cells[t][i] /= C_t.cells[0][t];
			}
		}


		// Calculation of gammas and digammas
		for(int t=0; t<T-1 ;t++)
		{
			denom = 0;
			for(int i=0; i<N ;i++)
			{
				for(int j=0; j<N ;j++)
				{
					denom += alpha.cells[t][i] * A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j];
				}
			}

			if(denom == 0)
				denom = 1;

			for(int i=0; i<N ;i++)
			{
				gammas.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					digammas[t].cells[i][j] = (alpha.cells[t][i] * A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j]) / denom;
					gammas.cells[t][i] += digammas[t].cells[i][j];
				}
			}
		}

		// Special case of gammas[T-1]
		denom = 0;
		for(int i=0; i<N ;i++)
		{
			denom += alpha.cells[T-1][i];
		}

		if(denom == 0)
			denom = 1;

		for(int i=0; i<N ;i++)
		{
			gammas.cells[T-1][i] = alpha.cells[T-1][i] / denom;
		}

		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);

		// Re-estimation of pi, A and B
		for(int i=0; i<N ;i++)
		{
			new_pi.cells[0][i] = gammas.cells[0][i];

			if(new_pi.cells[0][i] == 0)	
				new_pi.cells[0][i] =  epsilon;

			for(int j=0; j<K ;j++)
			{
				if(j < N)
				{
					numer = 0;
					denom = 0;
					for(int t=0; t<T-1 ;t++)
					{
						numer += digammas[t].cells[i][j];
						denom += gammas.cells[t][i];
					}
				
					if(denom == 0)
						denom = 1;

					if(numer != 0)
						new_A.cells[i][j] = numer/denom;
					else
						new_A.cells[i][j] = epsilon;
				}

				numer = 0;
				denom = 0;
				for(int t=0; t<T ;t++)
				{
					if(test_obs_seq[t] == j)
					{
						numer += gammas.cells[t][i];
					}
					denom += gammas.cells[t][i];
				}
		
				if(denom == 0)
					denom = 1;

				if(numer != 0)
					new_B.cells[i][j] = numer/denom;
				else
					new_B.cells[i][j] = epsilon;
			}
		}

		new_pi = new_pi.rowScaled();
		new_A = new_A.rowScaled();
		new_B = new_B.rowScaled();
	}

	
	void runForwardAlgorithm()
	{
		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][train_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][train_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "alpha" << std::endl;
		// alpha.displayFullInfo();
	}



	void runBackwardAlgorithm()
	{
		// Make beta equal to the scaling factors
		for(int i=0; i<N ;i++)
		{
			if(C_t.cells[0][T-1] != 0)
				beta.cells[T-1][i] = C_t.cells[0][T-1];
		}

		// Beta-pass
		for(int t=T-2; t>=0 ;t--)
		{
			for (int i = 0; i<N ;i++)
			{
				beta.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					beta.cells[t][i] += A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j];
				}
				
				if(C_t.cells[0][t] != 0.0)
					beta.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "beta" << std::endl;
		// beta.displayFullInfo();

		// Calculation of gammas and digammas
		double denom;
		for(int t=0; t<T-1 ;t++)
		{
			denom = 0;
			for(int i=0; i<N ;i++)
			{
				for(int j=0; j<N ;j++)
				{
					denom += alpha.cells[t][i] * A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j];
				}
			}

			if(denom == 0)
				denom = 1;

			for(int i=0; i<N ;i++)
			{
				gammas.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					digammas[t].cells[i][j] = (alpha.cells[t][i] * A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j]) / denom;
					gammas.cells[t][i] += digammas[t].cells[i][j];
				}
			}
		}

		// Special case of gammas[T-1]
		denom = 0;
		for(int i=0; i<N ;i++)
		{
			denom += alpha.cells[T-1][i];
		}

		if(denom == 0)
			denom = 1;

		for(int i=0; i<N ;i++)
		{
			gammas.cells[T-1][i] = alpha.cells[T-1][i] / denom;
		}

		// std::cerr << "gammas" << std::endl;
		// gammas.displayFullInfo();
	}




	void reestimateModel()
	{
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		double numer, denom;

		// Re-estimation of pi
		for(int i=0; i<N ;i++)
		{
			new_pi.cells[0][i] = gammas.cells[0][i];
		}

		// cout << "New pi" << endl;
		// new_pi.displayFullInfo();

		// Re-estimation of A
		for(int i=0; i<N ;i++)
		{
			for(int j=0; j<N ;j++)
			{
				numer = 0;
				denom = 0;
				for(int t=0; t<T-1 ;t++)
				{
					numer += digammas[t].cells[i][j];
					denom += gammas.cells[t][i];
				}

				if(denom == 0)
					denom = 1;

				new_A.cells[i][j] = numer/denom;
			}
		}

		// cout << "New A" << endl;
		// new_A.displayFullInfo();

		// Re-estimation of B
		for(int i=0; i<N ;i++)
		{
			for(int j=0; j<K ;j++)
			{
				numer = 0;
				denom = 0;
				for(int t=0; t<T ;t++)
				{
					if(train_obs_seq[t] == j)
					{
						numer += gammas.cells[t][i];
					}
					denom += gammas.cells[t][i];
				}

				if(denom == 0)
					denom = 1;

				if(numer != 0)
					new_B.cells[i][j] = numer/denom;
				else
					new_B.cells[i][j] = epsilon;
			}
		}

		// cout << "New B" << endl;
		// new_B.displayFullInfo();

		 new_pi = new_pi.rowScaled();
		 new_A = new_A.rowScaled();
		 new_B = new_B.rowScaled();;
	}




	void runViterbiAlgorithm()
	{
		N = pi.cols;
		K = B.cols;
		Matrix delta(1, N);

		delta = pi.elementWiseProduct(B.col(train_obs_seq[0]).T());
		deltas.push_back(delta.copy());

		int idx, max_idx;
		Matrix max_indices(N,T-1);
		max_indices.fillWithValue(-1.0);
		double value, max_value;

		for (int t = 1; t < T; t++)
		{
			delta = new Matrix(1, N);
			for(int i = 0; i < N ;i++)
			{
				value = -1;
				max_value = -1;
				max_idx = -1;
				for (int j = 0; j < N; j++)
				{
					value = deltas[t-1].cells[0][j] * A.cells[j][i] * B.cells[i][train_obs_seq[t]];
					if (value > max_value)
					{
						max_value = value;
						max_idx = j;
					}
				}
				delta.cells[0][i] = max_value;
				max_indices.cells[i][t-1] = (double)max_idx;
			}
			deltas.push_back(delta.copy());
		}

		// Backtrack hidden states 
		max_value = -1;
		max_idx = -1;
	  	for (int i = 0; i<N ; i++) 
		{
			 if(deltas[T-1].cells[0][i] > max_value)
			 {
				 max_value = deltas[T-1].cells[0][i];
				 max_idx = i;
			 }
		}
		hidden_states_seq[T-1] = max_idx;
		
		for (int i = T-1; i>0 ; i--) 
		{
			hidden_states_seq[i-1] = (int) max_indices.cells[hidden_states_seq[i]][i-1];
		}
	}


	void runBaumWelch()
	{
		T = train_obs_seq.size();
		iter = 0;
		reinitializeBaumWelchParams();
		while (iter<max_iter)
		{
			runForwardAlgorithm();
			runBackwardAlgorithm();
			reestimateModel();

			iter++;

			A = new_A.copy();
			B = new_B.copy();
			pi = new_pi.copy();
		}
		// std::cerr << "Baum-Welch iterations = " << iter << std::endl;
	}

	double sumCt()
	{
		double tmp = 0.0;
		for (int i = 0; i < T - 1; i++)
		{
			tmp += C_t.cells[0][i];
		}
		return tmp;
	}

};


#endif
