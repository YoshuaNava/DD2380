#ifndef _MATRIX_HPP_
#define _MATRIX_HPP_


#include <vector>
#include <iostream>
#include <math.h>
#include <random>



class Matrix
{
public:
	int rows, cols;
	double **cells;

	Matrix()
	{

	}

	Matrix(int rows, int cols)
	{
		this->rows = rows;
		this->cols = cols;
		cells = new double*[rows];
		for (int i = 0; i < rows; ++i)
		{
			cells[i] = new double[cols];
			for (int j = 0; j < cols; ++j)
			{
				cells[i][j] = 0;
			}
		}
	}

	// Operator * for conventional matrix sum
	Matrix operator+ (const Matrix& m2)
	{
		Matrix result(this->rows, this->cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < this->cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}

	// Operator / for conventional matrix-scalar division
	Matrix operator/ (double divisor)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] / divisor;
			}
		}

		return result;
	}

	// Operator / for conventional matrix-scalar division
	Matrix operator/= (double divisor)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] / divisor;
			}
		}

		return result;
	}


	// Operator += for conventional matrix sum
	Matrix operator+= (const Matrix& m2)
	{
		Matrix result(this->rows, this->cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < this->cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator * for conventional matrix multiplication
	Matrix operator* (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		double sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator *= for conventional matrix multiplication
	Matrix operator*= (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		double sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2
	Matrix& operator= (const Matrix& m2)
	{
		cells = m2.cells;
		rows = m2.rows;
		cols = m2.cols;

		return *this;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2.
	// Necessary for using the constructor after variable declaration
	Matrix& operator= (const Matrix* m2)
	{
		cells = m2->cells;
		rows = m2->rows;
		cols = m2->cols;

		return *this;
	}


	// Element wise product
	Matrix elementWiseProduct(const Matrix& m2)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				result.cells[i][k] = cells[i][k] * m2.cells[i][k];
			}
		}

		return result;
	}

	// This function returns a copy of this matrix
	Matrix copy()
	{
		Matrix copy_matrix(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				copy_matrix.cells[i][k] = cells[i][k];
			}
		}

		return copy_matrix;
	}

	// This function returns a copy of the row of index 'idx'
	Matrix row(int idx)
	{
		Matrix result(1, cols);
		for (int i = 0; i < cols; ++i)
		{
			result.cells[0][i] = cells[idx][i];
		}

		return result;
	}

	// This function returns the sum of the row of index 'idx'
	double rowSum(int idx)
	{
		double sum = 0;
		for (int i = 0; i < cols; ++i)
		{
			sum += cells[idx][i];
		}

		return sum;
	}

	int rowMaxIndex(int idx)
	{
		double max_value = 0;
		int max_idx = 0;
		for (int i = 0; i < cols; ++i)
		{
			if (cells[idx][i] > max_value)
			{
				max_value = cells[idx][i];
				max_idx = i;
			}
		}

		return max_idx;
	}


	// This function returns a copy of the maximum element of row of index 'idx'
	double rowMaxValue(int idx)
	{
		double max_value = 0;
		for (int i = 0; i < cols; ++i)
		{
			if (cells[idx][i] > max_value)
				max_value = cells[idx][i];
		}

		return max_value;
	}

	// This function returns a copy of the column of index 'idx'
	Matrix col(int idx)
	{
		Matrix result(rows, 1);
		for (int i = 0; i < rows; ++i)
		{
			result.cells[i][0] = cells[i][idx];
		}

		return result;
	}

	// This function returns the sum of the column of index 'idx'
	double colSum(int idx)
	{
		double sum = 0;
		for (int i = 0; i < rows; ++i)
		{
			sum += cells[i][idx];
		}

		return sum;
	}

	int colMaxIndex(int idx)
	{
		double max_value = 0;
		int max_idx = 0;
		for (int i = 0; i < rows; ++i)
		{
			if (cells[i][idx] > max_value)
			{
				max_value = cells[i][idx];
				max_idx = i;
			}
		}

		return max_idx;
	}

	// This function returns a copy of the maximum element of column of index 'idx'
	double colMaxValue(int idx)
	{
		double max_value = 0;
		for (int i = 0; i < rows; ++i)
		{
			if (cells[i][idx] > max_value)
				max_value = cells[i][idx];
		}

		return max_value;
	}

	// This function returns the transpose of the matrix
	Matrix T()
	{
		Matrix result(cols, rows);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[j][i] = cells[i][j];
			}
		}

		return result;
	}


	// This procedure fills the matrix with a given doubleing-point value
	void fillWithValue(double value)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = value;
			}
		}
	}

	Matrix rowScaled()
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			double sum_row = rowSum(i);
			if(sum_row != 0)
			{
				for (int j = 0; j < cols; j++)
				{
					result.cells[i][j] = cells[i][j] / sum_row;
				}
			}
		}
		return result;
	}

	Matrix colScaled()
	{
		Matrix result(rows, cols);
		for (int j = 0; j < cols; j++)
		{
			double sum_col = colSum(j);
			if(sum_col != 0)
			{
				for (int i = 0; i < rows; i++)		
				{
					result.cells[i][j] = cells[i][j] / sum_col;
				}
			}
		}
		return result;
	}


	// This procedure prints to stdout the values of the matrix cells, in one line. Kattis format!
	void displayOneLine()
	{
		std::cerr << rows << " " << cols << " ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				std::cerr << cells[i][j] << " ";
			}
		}
	}

	// This procedure prints to stdout the number of rows and columns of the matrix, and the values contained in it
	void displayFullInfo()
	{
		std::cerr << "Rows = " << rows << std::endl;
		std::cerr << "Columns = " << cols << std::endl;
		std::cerr << "Cell values = " << std::endl;
		std::string cell_spacing = "     ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (j == 0)
					std::cerr << "[" << cell_spacing.c_str();
				std::cerr << cells[i][j] << cell_spacing.c_str();
				if (j == cols - 1)
					std::cerr << "]";
			}
			std::cerr << std::endl;
		}
	}
};


#endif
