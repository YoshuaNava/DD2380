// HMM4.cpp
/* **************************************************************************
Authors: Kevin Holdcroft (kevinhol@kth.se) and Yoshua Nava (yoshua@kth.se)
Group ID: A35
Master's Programme: Systems, Control and Robotics, Year 2
************************************************************************** */

#include <stdio.h>
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;


class Matrix
{
public:
	int rows, cols;
	float **cells;

	Matrix()
	{

	}

	Matrix(int rows, int cols)
	{
		this->rows = rows;
		this->cols = cols;
		cells = new float*[rows];
		for (int i = 0; i < rows; ++i)
		{
			cells[i] = new float[cols];
			for (int j = 0; j < cols; ++j)
			{
				cells[i][j] = 0;
			}
		}
	}

	// Operator * for conventional matrix sum
	Matrix operator+ (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator += for conventional matrix sum
	Matrix operator+= (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator * for conventional matrix multiplication
	Matrix operator* (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator *= for conventional matrix multiplication
	Matrix operator*= (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2
	Matrix& operator= (const Matrix& m2)
	{
		cells = m2.cells;
		rows = m2.rows;
		cols = m2.cols;

		return *this;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2.
	// Necessary for using the constructor after variable declaration
	Matrix& operator= (const Matrix* m2)
	{
		cells = m2->cells;
		rows = m2->rows;
		cols = m2->cols;

		return *this;
	}


	// Element wise product
	Matrix elementWiseProduct(const Matrix& m2)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				result.cells[i][k] = cells[i][k] * m2.cells[i][k];
			}
		}

		return result;
	}

	// This function returns a copy of this matrix
	Matrix copy()
	{
		Matrix copy_matrix(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				copy_matrix.cells[i][k] = cells[i][k];
			}
		}

		return copy_matrix;
	}

	// This function returns a copy of the row of index 'idx'
	Matrix row(int idx)
	{
		Matrix result(1, cols);
		for (int i = 0; i < cols; ++i)
		{
			result.cells[0][i] = cells[idx][i];
		}

		return result;
	}

	// This function returns a copy of the column of index 'idx'
	Matrix col(int idx)
	{
		Matrix result(rows, 1);
		for (int i = 0; i < rows; ++i)
		{
			result.cells[i][0] = cells[i][idx];
		}

		return result;
	}

	// This function returns the transpose of the matrix
	Matrix T()
	{
		Matrix result(cols, rows);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[j][i] = cells[i][j];
			}
		}

		return result;
	}

	// This procedure reads from cin the values of the matrix
	void fillFromStringInput()
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cin >> cells[i][j];
			}
		}
	}

	// This procedure fills the matrix with a given floating-point value
	void fillWithValue(float value)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = value;
			}
		}
	}

	// This procedure prints to stdout the values of the matrix cells, in one line. Kattis format!
	void displayOneLine()
	{
		cout << rows << " " << cols << " ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cout << cells[i][j] << " ";
			}
		}
	}

	// This procedure prints to stdout the number of rows and columns of the matrix, and the values contained in it
	void displayFullInfo()
	{
		cout << "Rows = " << rows << endl;
		cout << "Columns = " << cols << endl;
		cout << "Cell values = " << endl;
		string cell_spacing = "     ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (j == 0)
					cout << "[" << cell_spacing.c_str();
				cout << cells[i][j] << cell_spacing.c_str();
				if (j == cols - 1)
					cout << "]";
			}
			cout << endl;
		}
		cout << "\n" << endl;
	}
};



// This parameter controls the amount of information displayed to the user
bool verbose = false;

// HMM Parameters
Matrix A, B, pi;
int* observations_seq;
Matrix alpha, beta;
int T, N, K;
vector<Matrix> digammas;
Matrix C_t, gammas, new_A, new_B, new_pi;
int max_iter = 100;
int iter = 0;
float log_prob, old_log_prob = -10000000000;


// Read matrices values from stdin
void receiveMatricesSTDInput()
{
	int rows, cols;

	cin >> rows;
	cin >> cols;
	A = new Matrix(rows, cols);
	A.fillFromStringInput();

	cin >> rows;
	cin >> cols;
	B = new Matrix(rows, cols);
	B.fillFromStringInput();

	cin >> rows;
	cin >> cols;
	pi = new Matrix(rows, cols);
	pi.fillFromStringInput();

	cin >> T;
	observations_seq = new int[T];
	for (int i = 0; i<T; i++)
	{
		cin >> observations_seq[i];
	}


	N = pi.cols;
	K = B.cols;
	C_t = new Matrix(1, T);
	alpha = new Matrix(T, N);
	beta = new Matrix(T, N);
	gammas = new Matrix(T, N);
	for(int t=0; t<T-1 ;t++)
	{
		Matrix tmp(N, N);
		digammas.push_back(tmp);
	}
}



// Print the results of this program to stdout
void displayResults()
{
	if (verbose)
	{
		cout << "\n" << endl;
		cout << "T = " << T << endl;
		cout << "N = " << N << endl;
		cout << "K = " << K << endl;
		cout << "Transition matrix" << endl;
		A.displayFullInfo();
		cout << "Emission matrix" << endl;
		B.displayFullInfo();
		cout << "Initial state" << endl;
		pi.displayFullInfo();

		cout << "Observation sequence" << endl;
		for (int i = 0; i<T; i++)
		{
			cout << observations_seq[i] << " ";
		}
		cout << "\n";

		cout << "\n***********************	RESULTS	 ***********************" << endl;
		cout << "\n";
	}
	else
	{
		A.displayOneLine();
		cout << "\n";
		B.displayOneLine();
	}
}


// Calculate the probability of the observations given the set Lambda. HMM1 objective.
void runForwardAlgorithm()
{
	C_t.cells[0][0] = 0;

	// Compute alpha_0
	for(int i=0; i<N ;i++)
	{
		alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][observations_seq[0]];
		C_t.cells[0][0] += alpha.cells[0][i];
	}

	// Scale alpha_0
	if(C_t.cells[0][0] != 0)
	{
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] /= C_t.cells[0][0];
		}
	}

	// Alpha-pass
	for(int t=1; t<T ;t++)
	{
		C_t.cells[0][t] = 0;
		for(int i=0; i<N ;i++)
		{
			alpha.cells[t][i] = 0;
			for(int j=0; j<N ;j++)
			{
				alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
			}
			alpha.cells[t][i] *= B.cells[i][observations_seq[t]];
			C_t.cells[0][t] += alpha.cells[t][i];
		}

		for(int i=0; i<N ;i++)
		{
			if(C_t.cells[0][t] != 0.0)
				alpha.cells[t][i] /= C_t.cells[0][t];
		}
	}

	// cout << "alpha" << endl;
	// alpha.displayFullInfo();
}


void runBackwardAlgorithm()
{
	// Make beta equal to the scaling factors
	for(int i=0; i<N ;i++)
	{
		beta.cells[T-1][i] = C_t.cells[0][T-1];
	}

	// Beta-pass
	for(int t=T-2; t>=0 ;t--)
	{
		for (int i = 0; i<N ;i++)
		{
			beta.cells[t][i] = 0;
			for(int j=0; j<N ;j++)
			{
				beta.cells[t][i] += A.cells[i][j] * B.cells[j][observations_seq[t+1]] * beta.cells[t+1][j];
			}
			
			if(C_t.cells[0][t] != 0.0)
				beta.cells[t][i] /= C_t.cells[0][t];
		}
	}

	// cout << "beta" << endl;
	// beta.displayFullInfo();

	// Calculation of gammas and digammas
	float denom;
	for(int t=0; t<T-1 ;t++)
	{
		denom = 0;
		for(int i=0; i<N ;i++)
		{
			for(int j=0; j<N ;j++)
			{
				denom += alpha.cells[t][i] * A.cells[i][j] * B.cells[j][observations_seq[t+1]] * beta.cells[t+1][j];
			}
		}

		if(denom == 0)
			denom = 1;

		for(int i=0; i<N ;i++)
		{
			gammas.cells[t][i] = 0;
			for(int j=0; j<N ;j++)
			{
				digammas[t].cells[i][j] = (alpha.cells[t][i] * A.cells[i][j] * B.cells[j][observations_seq[t+1]] * beta.cells[t+1][j]) / denom;
				gammas.cells[t][i] += digammas[t].cells[i][j];
			}
		}
	}

	// Special case of gammas[T-1]
	denom = 0;
	for(int i=0; i<N ;i++)
	{
		denom += alpha.cells[T-1][i];
	}

	if(denom == 0)
		denom = 1;

	for(int i=0; i<N ;i++)
	{
		gammas.cells[T-1][i] = alpha.cells[T-1][i] / denom;
	}
}



void reestimateModel()
{
	new_pi = new Matrix(1, N);
	new_A = new Matrix(N, N);
	new_B = new Matrix(N, K);
	float numer, denom;

	// Re-estimation of pi
	for(int i=0; i<N ;i++)
	{
		new_pi.cells[0][i] = gammas.cells[0][i];
	}

	// cout << "New pi" << endl;
	// new_pi.displayFullInfo();

	// Re-estimation of A
	for(int i=0; i<N ;i++)
	{
		for(int j=0; j<N ;j++)
		{
			numer = 0;
			denom = 0;
			for(int t=0; t<T-1 ;t++)
			{
				numer += digammas[t].cells[i][j];
				denom += gammas.cells[t][i];
			}
		
			if(denom == 0)
				denom = 1;

			new_A.cells[i][j] = numer/denom;
		}
	}

	// cout << "New A" << endl;
	// new_A.displayFullInfo();

	// Re-estimation of B
	for(int i=0; i<N ;i++)
	{
		for(int j=0; j<K ;j++)
		{
			numer = 0;
			denom = 0;
			for(int t=0; t<T ;t++)
			{
				if(observations_seq[t] == j)
				{
					numer += gammas.cells[t][i];
				}
				denom += gammas.cells[t][i];
			}
	
			if(denom == 0)
				denom = 1;

			new_B.cells[i][j] = numer/denom;
		}
	}

	// cout << "New B" << endl;
	// new_B.displayFullInfo();
}



int main()
{
	receiveMatricesSTDInput();
	bool valid_iter;

	// while((iter<max_iter) && (log_prob > old_log_prob))
	while(iter<max_iter)
	{
		valid_iter = true;
		old_log_prob = log_prob;

		runForwardAlgorithm();
		runBackwardAlgorithm();
		reestimateModel();

		iter++;
		log_prob = 0;
		for(int t=0; t<T ;t++)
		{
			log_prob += log(C_t.cells[0][t]);
		}
		log_prob *= -1.0;


		A = new_A.copy();
		B = new_B.copy();
		pi = new_pi.copy();
	}

	displayResults();


	return EXIT_SUCCESS;
}