// HMM1.cpp
/* **************************************************************************
  Authors: Kevin Holdcroft (kevinhol@kth.se) and Yoshua Nava (yoshua@kth.se)
  Group ID: A35
  Master's Programme: Systems, Control and Robotics, Year 2
************************************************************************** */

#include <stdio.h>
#include <iostream>

using namespace std;


class Matrix
{
public:
	int rows, cols;
	float **cells;

	Matrix()
	{

	}

	Matrix(int rows, int cols)
	{
		this->rows = rows;
		this->cols = cols;
		cells = new float*[rows];
		for (int i = 0; i < rows; ++i)
		{
			cells[i] = new float[cols];
			for (int j = 0; j < cols; ++j)
			{
				cells[i][j] = 0;
			}
		}
	}

	// Operator * for conventional matrix sum
	Matrix operator+ (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator += for conventional matrix sum
	Matrix operator+= (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}
	

	// Operator * for conventional matrix multiplication
	Matrix operator* (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for(int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator *= for conventional matrix multiplication
	Matrix operator*= (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for(int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2
	Matrix& operator= (const Matrix& m2)
	{
		cells = m2.cells;
		rows = m2.rows;
		cols = m2.cols;

		return *this;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2.
	// Necessary for using the constructor after variable declaration
	Matrix& operator= (const Matrix* m2)
	{
		cells = m2->cells;
		rows = m2->rows;
		cols = m2->cols;

		return *this;
	}


	// Element wise product
	Matrix elementWiseProduct(const Matrix& m2)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				result.cells[i][k] = cells[i][k] * m2.cells[i][k];
			}
		}

		return result;
	}

	// This function returns a copy of this matrix
	Matrix copy()
	{
		Matrix copy_matrix(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				copy_matrix.cells[i][k] = cells[i][k];
			}
		}

		return copy_matrix;
	}

	// This function returns a copy of the row of index 'idx'
	Matrix row(int idx)
	{
		Matrix result(1, cols);
		for (int i = 0; i < cols; ++i)
		{
			result.cells[0][i] = cells[idx][i];
		}

		return result;
	}

	// This function returns a copy of the column of index 'idx'
	Matrix col(int idx)
	{
		Matrix result(rows, 1);
		for (int i = 0; i < rows; ++i)
		{
			result.cells[i][0] = cells[i][idx];
		}

		return result;
	}

	// This function returns the transpose of the matrix
	Matrix T()
	{
		Matrix result(cols, rows);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[j][i] = cells[i][j];
			}
		}

		return result;
	}

	// This procedure reads from cin the values of the matrix
	void fillFromStringInput()
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cin >> cells[i][j];
			}
		}
	}

	// This procedure fills the matrix with a given floating-point value
	void fillWithValue(float value)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = value;
			}
		}
	}

	// This procedure prints to stdout the values of the matrix cells, in one line. Kattis format!
	void displayOneLine()
	{
		cout << rows << " " << cols << " ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cout << cells[i][j] << " ";
			}
		}
	}

	// This procedure prints to stdout the number of rows and columns of the matrix, and the values contained in it
	void displayFullInfo()
	{
		cout << "Rows = " << rows << endl;
		cout << "Columns = " << cols << endl;
		cout << "Cell values = " << endl;
		string cell_spacing = "     ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (j == 0)
					cout << "[" << cell_spacing.c_str();
				cout << cells[i][j] << cell_spacing.c_str();
				if (j == cols - 1)
					cout << "]";
			}
			cout << endl;
		}
		cout << "\n" << endl;
	}
};



// This parameter controls the amount of information displayed to the user
bool verbose = false;

// HMM Parameters
Matrix A, B, pi;
int* observations_seq;
int T, N, K;
float prob_obs_lambda;


// Read matrices values from stdin
void receiveMatricesSTDInput()
{
	int rows, cols;

	cin >> rows;
	cin >> cols;
	A = new Matrix(rows, cols);
	A.fillFromStringInput();
	
	cin >> rows;
	cin >> cols;
	B = new Matrix(rows, cols);
	B.fillFromStringInput();
	
	cin >> rows;
	cin >> cols;
	pi = new Matrix(rows, cols);
	pi.fillFromStringInput();

	cin >> T;
	observations_seq = new int[T];
	for (int i=0; i<T ;i++)
	{
		cin >> observations_seq[i];
	}
}

// Print the results of this program to stdout
void displayResults()
{
	if (verbose)
	{
		cout << "\n" << endl;
		cout << "T = " << T << endl;
		cout << "N = " << N << endl;
		cout << "K = " << K << endl;
		cout << "Transition matrix" << endl;
		A.displayFullInfo();
		cout << "Emission matrix" << endl;
		B.displayFullInfo();
		cout << "Initial state" << endl;
		pi.displayFullInfo();

		cout << "Observation sequence" << endl;
		for (int i = 0; i<T; i++)
		{
			cout << observations_seq[i] << " ";
		}
		cout << "\n";

		cout << "\n***********************	RESULTS	 ***********************" << endl;
		cout << "Probability of observations given the set lambda" << endl;
		cout << prob_obs_lambda << endl;
	}
	else
	{
		cout << prob_obs_lambda;
	}
}


// Calculate the probability of the observations given the set Lambda. HMM1 objective.
void runForwardAlgorithm()
{
	N = pi.cols;
	K = B.cols;
	Matrix alpha(1, N);

	alpha = pi.elementWiseProduct(B.col(observations_seq[0]).T());
	/*cout << "alpha_" << 0 << endl;
	alpha.displayFullInfo();*/

	for (int t = 1; t < T; t++)
	{
		alpha = alpha * A;
		alpha = alpha.elementWiseProduct(B.col(observations_seq[t]).T());
		/*cout << "alpha_" << i << endl;\
		alpha.displayFullInfo();*/
	}

	for (int i = 0; i < N; i++)
	{
		prob_obs_lambda += alpha.cells[0][i];
	}
}


int main()
{
	receiveMatricesSTDInput();

	runForwardAlgorithm();

	displayResults();
	

	return EXIT_SUCCESS;
}