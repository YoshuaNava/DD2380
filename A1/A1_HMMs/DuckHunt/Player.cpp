#include "Player.hpp"
#include <cstdlib>
#include <iostream>
#include <algorithm>

namespace ducks
{

Player::Player()
{
}

Action Player::shoot(const GameState &pState, const Deadline &pDue)
{
    /*
     * Here you should write your clever algorithms to get the best action.
     * This skeleton never shoots.
     */

	// std::cerr << "Shoot!" << std::endl;

	// Initialization of HMMs
	if (hmms_initialized == false)
	{
		HiddenMarkovModel pigeon, raven, skylark, swallow, snipe, black_stork;
		pigeon.initialize("Pigeon", SPECIES_PIGEON);
		raven.initialize("Raven", SPECIES_RAVEN);
		skylark.initialize("Skylark", SPECIES_SKYLARK);
		swallow.initialize("Swallow", SPECIES_SWALLOW);
		snipe.initialize("Snipe", SPECIES_SNIPE);
		black_stork.initialize("Black Stork", SPECIES_BLACK_STORK);
		hmms_species.push_back(pigeon);
		hmms_species.push_back(raven);
		hmms_species.push_back(skylark);
		hmms_species.push_back(swallow);
		hmms_species.push_back(snipe);
		hmms_species.push_back(black_stork);
		hmms_initialized = true;
	}

	Action best_action = cDontShoot;

	int num_birds = pState.getNumBirds();
	EMovement best_move = MOVE_DEAD;
	int best_bird_idx=0, best_move_idx=0;
	double max_move_prob=-1;
	int min_shoots_failed = 100;
	
	return best_action;
	
	if(pState.getRound() > curr_round)
	{
		curr_round = pState.getRound();
		shoot_steps = 0;
		hmms_birds = std::vector<HiddenMarkovModel>();
		shoots_failed = new int[num_birds];
		for(int i=0; i<num_birds ;i++)
		{
			shoots_failed[i] = 0;
			HiddenMarkovModel bird_hmm;
			bird_hmm.max_iter = 250;
			bird_hmm.initialize("");
			hmms_birds.push_back(bird_hmm);
		}
	}
	if(shoot_steps == shoot_train_steps)
	{
		// learn
		for(int i=0; i<num_birds ;i++)
		{
			Bird curr_bird = pState.getBird(i);
			if(curr_bird.isAlive())
			{
				float max_species_prob = 0;
				hmms_birds[i].id = SPECIES_UNKNOWN;
				for (int j = 0; j < num_species; j++)
				{
					if (hmms_species[j].trained)
					{
						for(int t=0; t<curr_bird.getSeqLength() ;t++)
						{
							int obs = (int)(curr_bird.getObservation(t));
							hmms_species[j].test_obs_seq.push_back(obs);
						}		
						hmms_species[j].reinitializeBaumWelchParamsTest();
						hmms_species[j].runForwardAlgorithmTest();					
						if(hmms_species[j].C_t.cells[0][hmms_species[j].T - 1] > max_species_prob)
						{
							max_species_prob = hmms_species[j].C_t.cells[0][hmms_species[j].T - 1];
							hmms_birds[i].id = hmms_species[j].id;
						}
					}
				}


				for(int j=0; j<curr_bird.getSeqLength() ;j++)
				{
					int obs = (int)(curr_bird.getObservation(j));
					hmms_birds[i].train_obs_seq.push_back(obs);
				}
				hmms_birds[i].reinitializeLambda();
				hmms_birds[i].reinitializeBaumWelchParams();
				hmms_birds[i].runBaumWelch();
				hmms_birds[i].trained = true;
			}
		}
	}
	else if(shoot_steps > shoot_train_steps)
	{
		for(int i=0; i<num_birds ;i++)
		{
			Bird curr_bird = pState.getBird(i);
			if(curr_bird.isAlive())
			{
				hmms_birds[i].test_obs_seq.push_back((int)curr_bird.getLastObservation()); 
				hmms_birds[i].reinitializeBaumWelchParamsTest();
				hmms_birds[i].runFutureStateEstimation();

				Matrix prob_next_obs = hmms_birds[i].gammas.row(hmms_birds[i].test_obs_seq.size()-1) * (hmms_birds[i].new_A * hmms_birds[i].new_B);
				// std::cerr << "Matrices!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
				// prob_next_obs.displayFullInfo();
								

				if ((hmms_birds[i].id != SPECIES_BLACK_STORK) && (hmms_birds[i].id != SPECIES_UNKNOWN))
				{
					for(int j=0; j<hmms_birds[i].K ;j++) 
					{ 
						if((prob_next_obs.cells[0][j] > max_move_prob) && (min_shoots_failed >= shoots_failed[i]))
						// if(prob_next_obs.cells[0][j] > max_move_prob)
						{ 
							min_shoots_failed = shoots_failed[i];
							max_move_prob = prob_next_obs.cells[0][j]; 
							best_move_idx = j; 
							best_bird_idx = i;
						} 
					}
				}
			}
		}

		switch(best_move_idx)
		{
			case 0:
				best_move = MOVE_UP_LEFT;
				break;
			case 1:
				best_move = MOVE_UP;
				break;
			case 2:
				best_move = MOVE_UP_RIGHT;
				break;
			case 3:
				best_move = MOVE_LEFT;
				break;
			case 4:
				best_move = MOVE_STOPPED;
				break;
			case 5:
				best_move = MOVE_RIGHT;
				break;
			case 6:
				best_move = MOVE_DOWN_LEFT;
				break;
			case 7:
				best_move = MOVE_DOWN;
				break;
			case 8:
				best_move = MOVE_DOWN_RIGHT;
				break;
			default:
				best_move = MOVE_DEAD;
				break;
		}
	
		if(max_move_prob > shooting_thresh)
		{
			// std::cerr << "Shoot!" << std::endl;
			// std::cerr << "Bird " << best_bird_idx << "	move " << best_move << std::endl;
			// std::cerr << "Probability " << max_move_prob << std::endl;
			// std::cerr << "Shoots failed until now " << shoots_failed[best_bird_idx] << std::endl;
			shoots_fired++;
			shoots_failed[best_bird_idx]++;
			best_action = Action(best_bird_idx, best_move);
		}
	}
	
	shoot_steps++;

	return best_action;
}

std::vector<ESpecies> Player::guess(const GameState &pState, const Deadline &pDue)
{
    /*
     * Here you should write your clever algorithms to guess the species of each bird.
     * This skeleton makes no guesses, better safe than sorry!
     */

	//  lGuesses = std::vector<ESpecies>(pState.getNumBirds(), SPECIES_UNKNOWN);
	//  return lGuesses;

	std::cerr << "\nGuess!" << std::endl;


	
	// We guess in the first round that all birds are pigeons, to gather information
	int iterator = 0;
	if (pState.getRound() == 0)
	{
		lGuesses = std::vector<ESpecies>();
		for (int i = 0; i < pState.getNumBirds(); i++)
		{
			iterator % 5;
			switch (iterator)
			{
				case 0:
					lGuesses.push_back(SPECIES_PIGEON);
					break;
				case 1:
					lGuesses.push_back(SPECIES_RAVEN);
					break;
				case 2:
					lGuesses.push_back(SPECIES_SKYLARK);
					break;
				case 3:
					lGuesses.push_back(SPECIES_SWALLOW);
					break;
				case 4:
					lGuesses.push_back(SPECIES_SNIPE);
					break;
				case 5:
					lGuesses.push_back(SPECIES_BLACK_STORK);
					break;
			}
		}
		iterator++;
		return lGuesses;
	}



	// For round 1 and on, we get the observations for each bird, run the forward algorithm, and attempt to find the maximum
	// value of the probability of the observation sequence given the model
	lGuesses = std::vector<ESpecies>(pState.getNumBirds(), SPECIES_PIGEON);
	int num_birds = pState.getNumBirds();
	for (int i = 0; i < num_birds; i++)
	{
		Bird curr_bird = pState.getBird(i);
		// std::cerr << "\nBird " << i << std::endl;
		int T = curr_bird.getSeqLength();
		double max_obs_prob = 0;
		double max_conf = -1;
		double Ct = 0;
		for(int j=0; j<num_species ;j++)
		{
			if(hmms_species[j].trained == true)
			{
				hmms_species[j].test_obs_seq = std::vector<int>();
				for (int t = 0; t < T ; t++)
				{
					int obs = (int)(curr_bird.getObservation(t));
					if(obs != -1)
						hmms_species[j].test_obs_seq.push_back(obs);
					else
						break;
				}

				hmms_species[j].reinitializeBaumWelchParamsTest();
				hmms_species[j].runForwardAlgorithmTest();
				
				//std::cerr << "	" << hmms_species[j].bird_type << ":" << std::endl;
				Ct = hmms_species[j].C_t.cells[0][hmms_species[j].T - 1];
				Ct = hmms_species[j].sumCt();
				//std::cerr << "	Ct = " << Ct << std::endl;

				if (Ct > max_obs_prob)// && (species_model_accuracy[j] > max_conf))
				{
					max_conf = species_model_accuracy[j];
					max_obs_prob = Ct;
					lGuesses[i] = hmms_species[j].id;
				}
			}
		}

		// std::cerr << "Guess = " << lGuesses[i] << std::endl;
		num_guesses++;
	}
	
	return lGuesses;
}



void Player::hit(const GameState &pState, int pBird, const Deadline &pDue)
{
    /*
     * If you hit the bird you are trying to shoot, you will be notified through this function.
     */
	std::cerr << "HIT BIRD!!!" << std::endl;
	std::cerr << "Bird " << pBird << "	move " << pState.getBird(pBird).getLastObservation() << std::endl;
	std::cerr << "Most probable species " << hmms_birds[pBird].id << std::endl;
	birds_hit++;
	shoots_failed[pBird]--;
}



void Player::reveal(const GameState &pState, const std::vector<ESpecies> &pSpecies, const Deadline &pDue)
{
    /*
     * If you made any guesses, you will find out the true species of those birds in this function.
     */

	std::cerr << "\nReveal!" << std::endl;
	int num_birds = pState.getNumBirds();
	std::cerr << "Number of birds " << num_birds << std::endl;
	// std::fill(std::begin(species_guesses),std::begin(species_guesses)+num_species,0);
	// std::fill(std::begin(species_correct_guesses),std::begin(species_correct_guesses)+num_species,0);
	// std::fill(std::begin(specimens),std::begin(specimens)+num_species,0);

	if(pState.getRound() > 0)
	{
		for (int j=0; j < num_birds; j++) 
		{ 
			// std::cerr << "\nBird " << j << std::endl; 
			// std::cerr << " Species: " << pSpecies[j] << std::endl; 
			// std::cerr << " Guess: " << lGuesses[j] << std::endl;
			if(pSpecies[j] == lGuesses[j])
			{
				correct_guesses++;
				species_correct_guesses[pSpecies[j]]++;
			}
			species_guesses[lGuesses[j]]++;
			specimens[pSpecies[j]]++;
		}

		std::cerr << "Results up to round " << pState.getRound() << std::endl;
		std::cerr << "	" << num_guesses << " guesses, " << correct_guesses << " correct. Accuracy = " << (double)(100*correct_guesses)/(num_guesses) << "%" << std::endl;
		std::cerr << "	" << shoots_fired << " shots, " << birds_hit << " birds hit. Accuracy = " << (double)(100*birds_hit)/(shoots_fired) << "%" << std::endl;
		std::cerr << "	Guesses per species model: [    ";
		for (int i=0; i < num_species; i++) 
		{
			std::cerr << species_correct_guesses[i] << " / ";
			std::cerr << species_guesses[i] << " / ";
			std::cerr << specimens[i];
			if(i < num_species-1)
				std::cerr << ",    ";
			else
				std::cerr << "    ";
		}
		std::cerr << "]  (correct / guesses / specimens)" << std::endl;
		std::cerr << "	Accuracy per species model: [ ";

		for (int i=0; i < num_species; i++) 
		{ 
			if(specimens[i] != 0)
				species_model_accuracy[i] = (double)(species_correct_guesses[i])/(specimens[i]);

			if(species_model_accuracy[i] < prob_acc_thresh)
			{
				hmms_species[i].trained = false;
			}
			else
			{
				hmms_species[i].trained = true;
			}
			std::cerr << (100.0*species_model_accuracy[i]);

			if(i < num_species-1)
				std::cerr << "%, ";
			else
				std::cerr << "% ";
		}
		std::cerr << "]" << std::endl;
	}

	for (int i = 0; i < num_species; i++)
	{
		if(hmms_species[i].trained == false)
		{
			hmms_species[i].reinitializeLambda();

			if(hmms_species[i].train_obs_seq.size() <= hmms_species[i].max_obs)
			{
				for (int j = 0; j < num_birds; j++)
				{
					Bird curr_bird = pState.getBird(j);
					if (pSpecies[j] == hmms_species[i].id)
					{
						for(int k=0; k < curr_bird.getSeqLength() ;k++)
						{
							if((curr_bird.getObservation(k) >= 0) && (hmms_species[i].train_obs_seq.size() < hmms_species[i].max_obs)) 
							{
								int obs = (int) (curr_bird.getObservation(k));
								hmms_species[i].train_obs_seq.push_back(obs);
							}
							else
								break;
						}
					}
				}
			}

			if(hmms_species[i].train_obs_seq.size() > 0)
			{
				hmms_species[i].reinitializeLambda();
				hmms_species[i].reinitializeBaumWelchParams();
				hmms_species[i].runBaumWelch();
				hmms_species[i].trained = true;

				//if (hmms_species[i].id == SPECIES_SKYLARK)
				//{
				//	std::cerr << "\nSpecies " << i << " being re-trained" << std::endl; 
				//	std::cerr << "Observations " << hmms_species[i].train_obs_seq.size() << std::endl;
				//	std::cerr << "Matrices learned " << std::endl;
				//	hmms_species[i].pi.displayFullInfo();
				//	hmms_species[i].A.displayFullInfo();
				//	hmms_species[i].B.displayFullInfo();
				//}
			}
		}
	}




}


} /*namespace ducks*/
