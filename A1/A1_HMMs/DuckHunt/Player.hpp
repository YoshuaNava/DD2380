#ifndef _DUCKS_PLAYER_HPP_
#define _DUCKS_PLAYER_HPP_


#include <math.h>
#include <time.h>
#include <vector>
#include <iostream>
#include <random>
#include "Deadline.hpp"
#include "GameState.hpp"
#include "Action.hpp"


namespace ducks
{


class Matrix
{
public:
	int rows, cols;
	double **cells;

	Matrix()
	{

	}

	Matrix(int rows, int cols)
	{
		this->rows = rows;
		this->cols = cols;
		cells = new double*[rows];
		for (int i = 0; i < rows; ++i)
		{
			cells[i] = new double[cols];
			for (int j = 0; j < cols; ++j)
			{
				cells[i][j] = 0;
			}
		}
	}

	// Operator * for conventional matrix sum
	Matrix operator+ (const Matrix& m2)
	{
		Matrix result(this->rows, this->cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < this->cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}

	// Operator / for conventional matrix-scalar division
	Matrix operator/ (double divisor)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] / divisor;
			}
		}

		return result;
	}

	// Operator / for conventional matrix-scalar division
	Matrix operator/= (double divisor)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] / divisor;
			}
		}

		return result;
	}


	// Operator += for conventional matrix sum
	Matrix operator+= (const Matrix& m2)
	{
		Matrix result(this->rows, this->cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < this->cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator * for conventional matrix multiplication
	Matrix operator* (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		double sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator *= for conventional matrix multiplication
	Matrix operator*= (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		double sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2
	Matrix& operator= (const Matrix& m2)
	{
		cells = m2.cells;
		rows = m2.rows;
		cols = m2.cols;

		return *this;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2.
	// Necessary for using the constructor after variable declaration
	Matrix& operator= (const Matrix* m2)
	{
		cells = m2->cells;
		rows = m2->rows;
		cols = m2->cols;

		return *this;
	}


	// Element wise product
	Matrix elementWiseProduct(const Matrix& m2)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				result.cells[i][k] = cells[i][k] * m2.cells[i][k];
			}
		}

		return result;
	}

	// This function returns a copy of this matrix
	Matrix copy()
	{
		Matrix copy_matrix(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				copy_matrix.cells[i][k] = cells[i][k];
			}
		}

		return copy_matrix;
	}

	// This function returns a copy of the row of index 'idx'
	Matrix row(int idx)
	{
		Matrix result(1, cols);
		for (int i = 0; i < cols; ++i)
		{
			result.cells[0][i] = cells[idx][i];
		}

		return result;
	}

	// This function returns the sum of the row of index 'idx'
	double rowSum(int idx)
	{
		double sum = 0;
		for (int i = 0; i < cols; ++i)
		{
			sum += cells[idx][i];
		}

		return sum;
	}

	int rowMaxIndex(int idx)
	{
		double max_value = 0;
		int max_idx = 0;
		for (int i = 0; i < cols; ++i)
		{
			if (cells[idx][i] > max_value)
			{
				max_value = cells[idx][i];
				max_idx = i;
			}
		}

		return max_idx;
	}


	// This function returns a copy of the maximum element of row of index 'idx'
	double rowMaxValue(int idx)
	{
		double max_value = 0;
		for (int i = 0; i < cols; ++i)
		{
			if (cells[idx][i] > max_value)
				max_value = cells[idx][i];
		}

		return max_value;
	}

	// This function returns a copy of the column of index 'idx'
	Matrix col(int idx)
	{
		Matrix result(rows, 1);
		for (int i = 0; i < rows; ++i)
		{
			result.cells[i][0] = cells[i][idx];
		}

		return result;
	}

	// This function returns the sum of the column of index 'idx'
	double colSum(int idx)
	{
		double sum = 0;
		for (int i = 0; i < rows; ++i)
		{
			sum += cells[i][idx];
		}

		return sum;
	}

	int colMaxIndex(int idx)
	{
		double max_value = 0;
		int max_idx = 0;
		for (int i = 0; i < rows; ++i)
		{
			if (cells[i][idx] > max_value)
			{
				max_value = cells[i][idx];
				max_idx = i;
			}
		}

		return max_idx;
	}

	// This function returns a copy of the maximum element of column of index 'idx'
	double colMaxValue(int idx)
	{
		double max_value = 0;
		for (int i = 0; i < rows; ++i)
		{
			if (cells[i][idx] > max_value)
				max_value = cells[i][idx];
		}

		return max_value;
	}

	// This function returns the transpose of the matrix
	Matrix T()
	{
		Matrix result(cols, rows);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[j][i] = cells[i][j];
			}
		}

		return result;
	}


	// This procedure fills the matrix with a given doubleing-point value
	void fillWithValue(double value)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = value;
			}
		}
	}

	Matrix rowScaled()
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; i++)
		{
			double sum_row = rowSum(i);
			if(sum_row != 0)
			{
				for (int j = 0; j < cols; j++)
				{
					result.cells[i][j] = cells[i][j] / sum_row;
				}
			}
		}
		return result;
	}

	Matrix colScaled()
	{
		Matrix result(rows, cols);
		for (int j = 0; j < cols; j++)
		{
			double sum_col = colSum(j);
			if(sum_col != 0)
			{
				for (int i = 0; i < rows; i++)		
				{
					result.cells[i][j] = cells[i][j] / sum_col;
				}
			}
		}
		return result;
	}


	// This procedure prints to stdout the values of the matrix cells, in one line. Kattis format!
	void displayOneLine()
	{
		std::cerr << rows << " " << cols << " ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				std::cerr << cells[i][j] << " ";
			}
		}
	}

	// This procedure prints to stdout the number of rows and columns of the matrix, and the values contained in it
	void displayFullInfo()
	{
		std::cerr << "Rows = " << rows << std::endl;
		std::cerr << "Columns = " << cols << std::endl;
		std::cerr << "Cell values = " << std::endl;
		std::string cell_spacing = "     ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (j == 0)
					std::cerr << "[" << cell_spacing.c_str();
				std::cerr << cells[i][j] << cell_spacing.c_str();
				if (j == cols - 1)
					std::cerr << "]";
			}
			std::cerr << std::endl;
		}
	}
};



class HiddenMarkovModel
{
public:

	std::string bird_type;
	ESpecies id;
	Matrix A, B, pi;
	int T = 99;
	int N = 3;
	int K = 9;
	double epsilon = 0.000000001;
	std::vector<int> train_obs_seq;
	std::vector<int> test_obs_seq;
	int* hidden_states_seq;
	Matrix alpha, beta;
	std::vector<Matrix> digammas, deltas;
	Matrix C_t, gammas, new_A, new_B, new_pi;
	int max_obs = 500;
	int max_iter = 200;
	int iter = 0;
	bool trained = false;
	double log_prob, old_log_prob = -10000000;


	void initialize(std::string model_name, ESpecies species_id = SPECIES_UNKNOWN)
	{
		id = species_id;
		bird_type = model_name;

		reinitializeLambda();

		train_obs_seq = std::vector<int>();
		test_obs_seq = std::vector<int>();
		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}



	void reinitializeLambda()
	{
		pi = new Matrix(1, N);
		A = new Matrix(N, N);
		B = new Matrix(N, K);
		
		srand (time(NULL));

		double scalePi = 0.0;
		double scaleA = 0.0;
		double scaleB = 0.0;
		for (int i = 0; i < N; i++)
		{
			pi.cells[0][i] = (1.0f + 10.0f*((double)std::rand() / (double)(RAND_MAX))) / (double)N;
			scalePi += pi.cells[0][i];
			for (int j = 0; j < K; j++)
			{
				if (j < N)
				{
					A.cells[i][j] = (1.0f + ((double)std::rand() / (double)(RAND_MAX))) / ((double)N);

					if(i ==  j)
						A.cells[i][j] *= 2;

					scaleA += A.cells[i][j];
				}
					

				B.cells[i][j] = (1.0f + ((double) std::rand() / (double)(RAND_MAX)))/ (double) K;
				
				if(i ==  j)
					B.cells[i][j] *= 2;

				scaleB += B.cells[i][j];
			}
			for (int j = 0; j < K; j++)
			{
				if (j < N)
					A.cells[i][j] /= scaleA;
				B.cells[i][j] /= scaleB;
			}
			scaleA = 0;
			scaleB = 0;
		}
		pi = pi / scalePi;
	}

	void reinitializeBaumWelchParams()
	{
		if(train_obs_seq.size() > 0)
			T = train_obs_seq.size();

		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}


	void reinitializeBaumWelchParamsTest()
	{
		if(test_obs_seq.size() > 0)
			T = test_obs_seq.size();

		hidden_states_seq = new int[T];
		C_t = new Matrix(1, T);
		alpha = new Matrix(T, N);
		beta = new Matrix(T, N);
		gammas = new Matrix(T, N);
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		digammas = std::vector<Matrix>();
		for (int t = 0; t<T-1 ;t++)
		{
			Matrix tmp(N, N);
			digammas.push_back(tmp);
		}
	}

	void runForwardAlgorithmTest()
	{
		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][test_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][test_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "alpha" << std::endl;
		// alpha.displayFullInfo();
	}


	void runFutureStateEstimation()
	{
		double numer, denom;

		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][test_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][test_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}
	
			// Make beta equal to the scaling factors
		for(int i=0; i<N ;i++)
		{
			if(C_t.cells[0][T-1] != 0)
				beta.cells[T-1][i] = C_t.cells[0][T-1];
		}

		// Beta-pass
		for(int t=T-2; t>=0 ;t--)
		{
			for (int i = 0; i<N ;i++)
			{
				beta.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					beta.cells[t][i] += A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j];
				}
				
				if(C_t.cells[0][t] != 0.0)
					beta.cells[t][i] /= C_t.cells[0][t];
			}
		}


		// Calculation of gammas and digammas
		for(int t=0; t<T-1 ;t++)
		{
			denom = 0;
			for(int i=0; i<N ;i++)
			{
				for(int j=0; j<N ;j++)
				{
					denom += alpha.cells[t][i] * A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j];
				}
			}

			if(denom == 0)
				denom = 1;

			for(int i=0; i<N ;i++)
			{
				gammas.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					digammas[t].cells[i][j] = (alpha.cells[t][i] * A.cells[i][j] * B.cells[j][test_obs_seq[t+1]] * beta.cells[t+1][j]) / denom;
					gammas.cells[t][i] += digammas[t].cells[i][j];
				}
			}
		}

		// Special case of gammas[T-1]
		denom = 0;
		for(int i=0; i<N ;i++)
		{
			denom += alpha.cells[T-1][i];
		}

		if(denom == 0)
			denom = 1;

		for(int i=0; i<N ;i++)
		{
			gammas.cells[T-1][i] = alpha.cells[T-1][i] / denom;
		}

		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);

		// Re-estimation of pi, A and B
		for(int i=0; i<N ;i++)
		{
			new_pi.cells[0][i] = gammas.cells[0][i];

			if(new_pi.cells[0][i] == 0)	
				new_pi.cells[0][i] =  epsilon;

			for(int j=0; j<K ;j++)
			{
				if(j < N)
				{
					numer = 0;
					denom = 0;
					for(int t=0; t<T-1 ;t++)
					{
						numer += digammas[t].cells[i][j];
						denom += gammas.cells[t][i];
					}
				
					if(denom == 0)
						denom = 1;

					if(numer != 0)
						new_A.cells[i][j] = numer/denom;
					else
						new_A.cells[i][j] = epsilon;
				}

				numer = 0;
				denom = 0;
				for(int t=0; t<T ;t++)
				{
					if(test_obs_seq[t] == j)
					{
						numer += gammas.cells[t][i];
					}
					denom += gammas.cells[t][i];
				}
		
				if(denom == 0)
					denom = 1;

				if(numer != 0)
					new_B.cells[i][j] = numer/denom;
				else
					new_B.cells[i][j] = epsilon;
			}
		}

		new_pi = new_pi.rowScaled();
		new_A = new_A.rowScaled();
		new_B = new_B.rowScaled();
	}

	
	void runForwardAlgorithm()
	{
		C_t.cells[0][0] = 0;

		// Compute alpha_0
		for(int i=0; i<N ;i++)
		{
			alpha.cells[0][i] = pi.cells[0][i] * B.cells[i][train_obs_seq[0]];
			C_t.cells[0][0] += alpha.cells[0][i];
		}

		// Scale alpha_0
		if(C_t.cells[0][0] != 0)
		{
			for(int i=0; i<N ;i++)
			{
				alpha.cells[0][i] /= C_t.cells[0][0];
			}
		}

		// Alpha-pass
		for(int t=1; t<T ;t++)
		{
			C_t.cells[0][t] = 0;
			for(int i=0; i<N ;i++)
			{
				alpha.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					alpha.cells[t][i] += alpha.cells[t-1][j] * A.cells[j][i];
				}
				alpha.cells[t][i] *= B.cells[i][train_obs_seq[t]];
				C_t.cells[0][t] += alpha.cells[t][i];
			}

			for(int i=0; i<N ;i++)
			{
				if(C_t.cells[0][t] != 0.0)
					alpha.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "alpha" << std::endl;
		// alpha.displayFullInfo();
	}



	void runBackwardAlgorithm()
	{
		// Make beta equal to the scaling factors
		for(int i=0; i<N ;i++)
		{
			if(C_t.cells[0][T-1] != 0)
				beta.cells[T-1][i] = C_t.cells[0][T-1];
		}

		// Beta-pass
		for(int t=T-2; t>=0 ;t--)
		{
			for (int i = 0; i<N ;i++)
			{
				beta.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					beta.cells[t][i] += A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j];
				}
				
				if(C_t.cells[0][t] != 0.0)
					beta.cells[t][i] /= C_t.cells[0][t];
			}
		}

		// std::cerr << "beta" << std::endl;
		// beta.displayFullInfo();

		// Calculation of gammas and digammas
		double denom;
		for(int t=0; t<T-1 ;t++)
		{
			denom = 0;
			for(int i=0; i<N ;i++)
			{
				for(int j=0; j<N ;j++)
				{
					denom += alpha.cells[t][i] * A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j];
				}
			}

			if(denom == 0)
				denom = 1;

			for(int i=0; i<N ;i++)
			{
				gammas.cells[t][i] = 0;
				for(int j=0; j<N ;j++)
				{
					digammas[t].cells[i][j] = (alpha.cells[t][i] * A.cells[i][j] * B.cells[j][train_obs_seq[t+1]] * beta.cells[t+1][j]) / denom;
					gammas.cells[t][i] += digammas[t].cells[i][j];
				}
			}
		}

		// Special case of gammas[T-1]
		denom = 0;
		for(int i=0; i<N ;i++)
		{
			denom += alpha.cells[T-1][i];
		}

		if(denom == 0)
			denom = 1;

		for(int i=0; i<N ;i++)
		{
			gammas.cells[T-1][i] = alpha.cells[T-1][i] / denom;
		}

		// std::cerr << "gammas" << std::endl;
		// gammas.displayFullInfo();
	}



	void reestimateModel()
	{
		new_pi = new Matrix(1, N);
		new_A = new Matrix(N, N);
		new_B = new Matrix(N, K);
		double numer, denom;

		// Re-estimation of pi, A and B
		for(int i=0; i<N ;i++)
		{
			new_pi.cells[0][i] = gammas.cells[0][i];

			if(new_pi.cells[0][i] == 0)	
				new_pi.cells[0][i] =  epsilon;

			for(int j=0; j<K ;j++)
			{
				if(j < N)
				{
					numer = 0;
					denom = 0;
					for(int t=0; t<T-1 ;t++)
					{
						numer += digammas[t].cells[i][j];
						denom += gammas.cells[t][i];
					}
				
					if(denom == 0)
						denom = 1;

					if(numer != 0)
						new_A.cells[i][j] = numer/denom;
					else
						new_A.cells[i][j] = epsilon;
				}

				numer = 0;
				denom = 0;
				for(int t=0; t<T ;t++)
				{
					if(train_obs_seq[t] == j)
					{
						numer += gammas.cells[t][i];
					}
					denom += gammas.cells[t][i];
				}
		
				if(denom == 0)
					denom = 1;

				if(numer != 0)
					new_B.cells[i][j] = numer/denom;
				else
					new_B.cells[i][j] = epsilon;
			}
		}

		 new_pi = new_pi.rowScaled();
		 new_A = new_A.rowScaled();
		 new_B = new_B.rowScaled();;
	}



	void runViterbiAlgorithm()
	{
		N = pi.cols;
		K = B.cols;
		Matrix delta(1, N);

		delta = pi.elementWiseProduct(B.col(train_obs_seq[0]).T());
		deltas.push_back(delta.copy());

		int idx, max_idx;
		Matrix max_indices(N,T-1);
		max_indices.fillWithValue(-1.0);
		double value, max_value;

		for (int t = 1; t < T; t++)
		{
			delta = new Matrix(1, N);
			for(int i = 0; i < N ;i++)
			{
				value = -1;
				max_value = -1;
				max_idx = -1;
				for (int j = 0; j < N; j++)
				{
					value = deltas[t-1].cells[0][j] * A.cells[j][i] * B.cells[i][train_obs_seq[t]];
					if (value > max_value)
					{
						max_value = value;
						max_idx = j;
					}
				}
				delta.cells[0][i] = max_value;
				max_indices.cells[i][t-1] = (double)max_idx;
			}
			deltas.push_back(delta.copy());
		}

		// Backtrack hidden states 
		max_value = -1;
		max_idx = -1;
	  	for (int i = 0; i<N ; i++) 
		{
			 if(deltas[T-1].cells[0][i] > max_value)
			 {
				 max_value = deltas[T-1].cells[0][i];
				 max_idx = i;
			 }
		}
		hidden_states_seq[T-1] = max_idx;
		
		for (int i = T-1; i>0 ; i--) 
		{
			hidden_states_seq[i-1] = (int) max_indices.cells[hidden_states_seq[i]][i-1];
		}
	}


	void runBaumWelch()
	{
		T = train_obs_seq.size();
		iter = 0;
		reinitializeBaumWelchParams();
		// while((iter<max_iter) && (abs(log_prob - old_log_prob) > 0.001))
		while (iter<max_iter)
		{
			old_log_prob = log_prob;

			runForwardAlgorithm();
			runBackwardAlgorithm();
			reestimateModel();

			log_prob = 0;
			for(int t=0; t<T ;t++)
			{
				log_prob += log(1/C_t.cells[0][t]);
			}
			log_prob *= -1.0;
	

			iter++;

			A = new_A.copy();
			B = new_B.copy();
			pi = new_pi.copy();
		}
		// std::cerr << "Baum-Welch iterations = " << iter << std::endl;
	}

	double sumCt()
	{
		double tmp = 0.0;
		for (int i = 0; i < T - 1; i++)
		{
			tmp += C_t.cells[0][i];
		}
		return tmp;
	}

};



class Player
{
public:
    /**
     * Constructor
     * There is no data in the beginning, so not much should be done here.
     */
    Player();
    static const int num_species = 6;
	bool hmms_initialized = false;
	std::vector<ESpecies> lGuesses;
	int curr_round = -1;

    std::vector<HiddenMarkovModel> hmms_species;
    std::vector<HiddenMarkovModel> hmms_birds;
	std::vector<int> speciesCount = { 0, 0, 0, 0, 0, 0 };

	
	int specimens[num_species] = {0, 0, 0, 0, 0, 0};
	int species_guesses[num_species] = {0, 0, 0, 0, 0, 0};
	int species_correct_guesses[num_species] = {0, 0, 0, 0, 0, 0};
	double species_model_accuracy[num_species] = {0, 0, 0, 0, 0, 0};
	int* shoots_failed;
	int shoots_fired=0, birds_hit=0, num_guesses=0, correct_guesses=0;
	double prob_acc_thresh = 0.85;
	double shooting_thresh = 0.5;
	int shoot_train_steps = 60;
	int shoot_steps = 0;

	double max_diff = 1;
    /**
     * Shoot!
     *
     * This is the function where you start your work.
     *
     * You will receive a variable pState, which contains information about all
     * birds, both dead and alive. Each birds contains all past actions.
     *
     * The state also contains the scores for all players and the number of
     * time steps elapsed since the last time this function was called.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return the prediction of a bird we want to shoot at, or cDontShoot to pass
     */
    Action shoot(const GameState &pState, const Deadline &pDue);


    /**
     * Guess the species!
     * This function will be called at the end of each round, to give you
     * a chance to identify the species of the birds for extra points.
     *
     * Fill the vector with guesses for the all birds.
     * Use SPECIES_UNKNOWN to avoid guessing.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return a vector with guesses for all the birds
     */
    std::vector<ESpecies> guess(const GameState &pState, const Deadline &pDue);

    /**
     * If you hit the bird you were trying to shoot, you will be notified
     * through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pBird the bird you hit
     * @param pDue time before which we must have returned
     */
    void hit(const GameState &pState, int pBird, const Deadline &pDue);

    /**
     * If you made any guesses, you will find out the true species of those
     * birds through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pSpecies the vector with species
     * @param pDue time before which we must have returned
     */
    void reveal(const GameState &pState, const std::vector<ESpecies> &pSpecies, const Deadline &pDue);
};

} /*namespace ducks*/

#endif
