// HMM0.cpp
/* **************************************************************************
  Authors: Kevin Holdcroft (kevinhol@kth.se) and Yoshua Nava (yoshua@kth.se)
  Group ID: A35
  Master's Programme: Systems, Control and Robotics, Year 2
************************************************************************** */

#include <stdio.h>
#include <iostream>

using namespace std;


class Matrix
{
public:
	int rows, cols;
	float **cells;

	Matrix()
	{

	}

	Matrix(int rows, int cols)
	{
		this->rows = rows;
		this->cols = cols;
		cells = new float*[rows];
		for (int i = 0; i < rows; ++i)
		{
			cells[i] = new float[cols];
			for (int j = 0; j < cols; ++j)
			{
				cells[i][j] = 0;
			}
		}
	}

	// Operator * for conventional matrix sum
	Matrix operator+ (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator += for conventional matrix sum
	Matrix operator+= (const Matrix& m2)
	{
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				result.cells[i][j] = this->cells[i][j] + m2.cells[i][j];
			}
		}

		return result;
	}


	// Operator * for conventional matrix multiplication
	Matrix operator* (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator *= for conventional matrix multiplication
	Matrix operator*= (const Matrix& m2)
	{
		// Reference for this algorithm: https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm
		float sum = 0.0;
		Matrix result(this->rows, m2.cols);
		for (int i = 0; i < this->rows; i++)
		{
			for (int j = 0; j < m2.cols; j++)
			{
				sum = 0.0;
				for (int k = 0; k < this->cols; k++)
				{
					sum += this->cells[i][k] * m2.cells[k][j];
				}
				result.cells[i][j] = sum;
			}
		}

		return result;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2
	Matrix& operator= (const Matrix& m2)
	{
		cells = m2.cells;
		rows = m2.rows;
		cols = m2.cols;

		return *this;
	}

	// Operator = for setting the value of this object as that of another Matrix object m2.
	// Necessary for using the constructor after variable declaration
	Matrix& operator= (const Matrix* m2)
	{
		cells = m2->cells;
		rows = m2->rows;
		cols = m2->cols;

		return *this;
	}


	// Element wise product
	Matrix elementWiseProduct(const Matrix& m2)
	{
		Matrix result(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				result.cells[i][k] = cells[i][k] * m2.cells[i][k];
			}
		}

		return result;
	}

	// This function returns a copy of this matrix
	Matrix copy()
	{
		Matrix copy_matrix(rows, cols);
		for (int i = 0; i < rows; ++i)
		{
			for (int k = 0; k < cols; ++k)
			{
				copy_matrix.cells[i][k] = cells[i][k];
			}
		}

		return copy_matrix;
	}

	// This function returns a copy of the row of index 'idx'
	Matrix row(int idx)
	{
		Matrix result(1, cols);
		for (int i = 0; i < cols; ++i)
		{
			result.cells[0][i] = cells[idx][i];
		}

		return result;
	}

	// This function returns a copy of the column of index 'idx'
	Matrix col(int idx)
	{
		Matrix result(rows, 1);
		for (int i = 0; i < rows; ++i)
		{
			result.cells[i][0] = cells[i][idx];
		}

		return result;
	}

	// This function returns the transpose of the matrix
	Matrix T()
	{
		Matrix result(cols, rows);
		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < cols; j++)
			{
				result.cells[j][i] = cells[i][j];
			}
		}

		return result;
	}

	// This procedure reads from cin the values of the matrix
	void fillFromStringInput()
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cin >> cells[i][j];
			}
		}
	}

	// This procedure fills the matrix with a given floating-point value
	void fillWithValue(float value)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = value;
			}
		}
	}

	// This procedure prints to stdout the values of the matrix cells, in one line. Kattis format!
	void displayOneLine()
	{
		cout << rows << " " << cols << " ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cout << cells[i][j] << " ";
			}
		}
	}

	// This procedure prints to stdout the number of rows and columns of the matrix, and the values contained in it
	void displayFullInfo()
	{
		cout << "Rows = " << rows << endl;
		cout << "Columns = " << cols << endl;
		cout << "Cell values = " << endl;
		string cell_spacing = "     ";
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (j == 0)
					cout << "[" << cell_spacing.c_str();
				cout << cells[i][j] << cell_spacing.c_str();
				if (j == cols - 1)
					cout << "]";
			}
			cout << endl;
		}
		cout << "\n" << endl;
	}
};


// This parameter controls the amount of information displayed to the user
bool verbose = false;

// HMM Parameters
Matrix A, B, pi;
Matrix prob_observations;


// Read matrices values from stdin
void receiveMatricesSTDInput()
{
	int rows, cols;

	cin >> rows;
	cin >> cols;
	A = new Matrix(rows, cols);
	A.fillFromStringInput();
	
	cin >> rows;
	cin >> cols;
	B = new Matrix(rows, cols);
	B.fillFromStringInput();
	
	cin >> rows;
	cin >> cols;
	pi = new Matrix(rows, cols);
	pi.fillFromStringInput();
}

// Print the results of this program to stdout
void displayResults()
{
	if (verbose)
	{
		cout << "\n" << endl;
		cout << "Transition matrix" << endl;
		A.displayFullInfo();
		cout << "Emission matrix" << endl;
		B.displayFullInfo();
		cout << "Initial state" << endl;
		pi.displayFullInfo();

		cout << "\n***********************	RESULTS	 ***********************" << endl;
		cout << "Probability of observations" << endl;
		prob_observations.displayFullInfo();
	}
	else
	{
		prob_observations.displayOneLine();
	}
}


// Calculate the probability of the observations. HMM0 objective.
void calcProbObservations()
{
	int T, N, K;
	T = A.rows;
	N = pi.cols;
	K = B.cols;

	prob_observations = (pi * A) * B;
}


int main()
{
	receiveMatricesSTDInput();

	calcProbObservations();

	displayResults();
	

	return EXIT_SUCCESS;
}