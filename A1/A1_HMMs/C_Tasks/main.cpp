// main.cpp
/* **************************************************************************
Authors: Kevin Holdcroft (kevinhol@kth.se) and Yoshua Nava (yoshua@kth.se)
Group ID: A35
Master's Programme: Systems, Control and Robotics, Year 2
************************************************************************** */

#include <stdio.h>
#include <iostream>
#include <vector>
#include <math.h>
#include "Matrix.hpp"
#include "HMM.hpp"



// This parameter controls the amount of information displayed to the user
bool verbose = true;
// Original matrices to be used for questions 7, 8 and 9
Matrix original_A, original_B, original_pi;

// Learned matrices from question 7, with maximum amount of observations
Matrix final_A, final_B, final_pi;

// HMM Parameters
int max_iter = 100;
int iter = 0;
HiddenMarkovModel hmm;




// Print the results of this program to stdout
void displayResults()
{
	if (verbose)
	{
		std::cout << "\n" << std::endl;
		std::cout << "T = " << hmm.T << std::endl;
		std::cout << "N = " << hmm.N << std::endl;
		std::cout << "K = " << hmm.K << std::endl;
		std::cout << "Transition matrix" << std::endl;
		original_A.displayFullInfo();
		std::cout << "Emission matrix" << std::endl;
		original_B.displayFullInfo();
		std::cout << "Initial state" << std::endl;
		original_pi.displayFullInfo();

		std::cout << "Observation sequence" << std::endl;
		for (int i = 0; i<hmm.T; i++)
		{
			std::cout << hmm.train_obs_seq[i] << " ";
		}
		std::cout << "\n";

		std::cout << "\n***********************	RESULTS	 ***********************" << std::endl;
		hmm.A.displayFullInfo();
		std::cout << "\n";
		hmm.B.displayFullInfo();
		std::cout << "\n";
		hmm.pi.displayFullInfo();
	}
	else
	{
		hmm.A.displayOneLine();
		std::cout << "\n";
		hmm.B.displayOneLine();
	}
}



void runQuestion7()
{
	hmm.N = 3;
	hmm.K = 4;
	hmm.pi = new Matrix(1, hmm.N);
	hmm.pi.cells[0][0] = 0.3;
	hmm.pi.cells[0][1] = 0.2;
	hmm.pi.cells[0][2] = 0.5;

	hmm.A = new Matrix(hmm.N, hmm.N);
	hmm.A.cells[0][0] = 0.54;
	hmm.A.cells[0][1] = 0.26;
	hmm.A.cells[0][2] = 0.20;
	hmm.A.cells[1][0] = 0.19;
	hmm.A.cells[1][1] = 0.53;
	hmm.A.cells[1][2] = 0.28;
	hmm.A.cells[2][0] = 0.22;
	hmm.A.cells[2][1] = 0.18;
	hmm.A.cells[2][2] = 0.6;

	hmm.B = new Matrix(hmm.N, hmm.K);
	hmm.B.cells[0][0] = 0.5;
	hmm.B.cells[0][1] = 0.2;
	hmm.B.cells[0][2] = 0.11;
	hmm.B.cells[0][3] = 0.19;
	hmm.B.cells[1][0] = 0.22;
	hmm.B.cells[1][1] = 0.28;
	hmm.B.cells[1][2] = 0.23;
	hmm.B.cells[1][3] = 0.27;
	hmm.B.cells[2][0] = 0.19;
	hmm.B.cells[2][1] = 0.21;
	hmm.B.cells[2][2] = 0.15;
	hmm.B.cells[2][3] = 0.45;

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	std::cin >> hmm.T;
	// hmm.T = 600;
	max_iter = 200;
	hmm.train_obs_seq = std::vector<int>();
	for (int i = 0; i<hmm.T; i++)
	{
		int obs;
		std::cin >> obs;
		hmm.train_obs_seq.push_back(obs);
	}


	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}



void runQuestion8()
{
	hmm.N = 3;
	hmm.K = 4;
	hmm.reinitializeLambda();

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	max_iter = 200;
	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}




void runQuestion9()
{
	hmm.N = 1;
	hmm.K = 4;
	hmm.reinitializeLambda();

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	max_iter = 200;
	hmm.T = 200;
	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}



void runQuestion10a()
{
	hmm.N = 3;
	hmm.K = 4;
	hmm.reinitializeLambda();
	hmm.A = Matrix::eye(hmm.N, hmm.N);
	hmm.pi.cells[0][0] = 0;
	hmm.pi.cells[0][1] = 0;
	hmm.pi.cells[0][2] = 1.0;

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	max_iter = 200;
	hmm.T = 200;
	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}



void runQuestion10b()
{
	hmm.N = 3;
	hmm.K = 4;
	hmm.reinitializeLambda();
	hmm.A = Matrix::initializeUniform(hmm.N, hmm.N);
	hmm.B = Matrix::initializeUniform(hmm.N, hmm.K);
	hmm.pi = Matrix::initializeUniform(1, hmm.N);

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	max_iter = 200;
	hmm.T = 200;
	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}



void runQuestion10c()
{
	hmm.N = 3;
	hmm.K = 4;
	hmm.reinitializeLambda();
	hmm.A = final_A.copy();
	hmm.B = final_B.copy();
	hmm.pi = final_pi.copy();

	original_pi = hmm.pi.copy();
	original_A = hmm.A.copy();
	original_B = hmm.B.copy();

	max_iter = 200;
	hmm.T = 200;
	hmm.C_t = new Matrix(1, hmm.T);
	hmm.alpha = new Matrix(hmm.T, hmm.N);
	hmm.beta = new Matrix(hmm.T, hmm.N);
	hmm.gammas = new Matrix(hmm.T, hmm.N);
	for(int t=0; t<hmm.T-1 ;t++)
	{
		Matrix tmp(hmm.N, hmm.N);
		hmm.digammas.push_back(tmp);
	}

	hmm.runBaumWelch();
}


int main()
{

	std::cout << "################################################		Question 7		################################################" << std::endl;
	runQuestion7();
	displayResults();
	final_pi = hmm.pi.copy();
	final_A = hmm.A.copy();
	final_B = hmm.B.copy();

	
	std::cout << "################################################		Question 8		################################################" << std::endl;
	runQuestion8();
	displayResults();

	std::cout << "################################################		Question 9		################################################" << std::endl;
	runQuestion9();
	displayResults();

	std::cout << "################################################		Question 10a		################################################" << std::endl;
	runQuestion10a();
	displayResults();


	std::cout << "################################################		Question 10b		################################################" << std::endl;
	runQuestion10b();
	displayResults();


	std::cout << "################################################		Question 10c		################################################" << std::endl;
	runQuestion10c();
	displayResults();

	return EXIT_SUCCESS;
}